///:
/*****************************************************************************
 **                                                                         **
 **                               .======.                                  **
 **                               | INRI |                                  **
 **                               |      |                                  **
 **                               |      |                                  **
 **                      .========'      '========.                         **
 **                      |   _      xxxx      _   |                         **
 **                      |  /_;-.__ / _\  _.-;_\  |                         **
 **                      |     `-._`'`_/'`.-'     |                         **
 **                      '========.`\   /`========'                         **
 **                               | |  / |                                  **
 **                               |/-.(  |                                  **
 **                               |\_._\ |                                  **
 **                               | \ \`;|                                  **
 **                               |  > |/|                                  **
 **                               | / // |                                  **
 **                               | |//  |                                  **
 **                               | \(\  |                                  **
 **                               |  ``  |                                  **
 **                               |      |                                  **
 **                               |      |                                  **
 **                               |      |                                  **
 **                               |      |                                  **
 **                   \\    _  _\\| \//  |//_   _ \// _                     **
 **                  ^ `^`^ ^`` `^ ^` ``^^`  `^^` `^ `^                     **
 **                                                                         **
 **                       Copyright (c) 2014 Tong G.                        **
 **                          ALL RIGHTS RESERVED.                           **
 **                                                                         **
 ****************************************************************************/

#import <Foundation/Foundation.h>

enum { TEEncodingManagerNoStringEncoding = 0xFFFFFFFF };
enum { TEEncodingManagerWantsAutomaticTag = -1 };

#pragma mark -
#pragma mark Notification Names
NSString extern* const TEEncodingManagerEncodingsListChangedNotification;
NSString extern* const TEEncodingManagerEncodingEncodingsListChangedNotification;

#pragma mark -
#pragma mark User Default Keys
NSString extern* const TEEncodingManagerEncodingEncodings;

#pragma mark -
#pragma mark TEEncodingPopUpButtonCell
@interface TEEncodingPopUpButtonCell : NSPopUpButtonCell
@end // TEEncodingManagerNoStringEncoding

#pragma mark -
#pragma mark TEEncodingManager
@interface TEEncodingManager : NSObject

@property ( readwrite, retain ) IBOutlet NSMatrix* _encodingMatrix;
@property ( readwrite, retain ) NSArray* _encodings;

/* There is just one instance... */
+ ( TEEncodingManager* ) sharedEncodingManager;

/* List of encoding that should in encoding lists */
- ( NSArray* ) enabledEncodings;

/* Empties then initializes the supplied popup with the supported encodings. */
- ( void ) setupPopUpCell: ( TEEncodingPopUpButtonCell* ) _Button
         selectedEncoding: ( NSStringEncoding ) _SelectedEncoding
         withDefaultEntry: ( BOOL ) _IncludeDefaultItem;

/* Action methods for bringing up and dealing with changes in the encoding list panel */
- ( IBAction ) showPanel: ( id ) _Sender;
- ( IBAction ) encodingListChanged: ( id ) _Sender;
- ( IBAction ) clearAll: ( id ) _Sender;
- ( IBAction ) selectAll: ( id ) _Sender;
- ( IBAction ) revertToDefault: ( id ) _Sender;

/* Internal method to save and communicate changes to encoding list */
- ( void ) noteEncodingListChange: ( BOOL ) _WriteDefault
                       updateList: ( BOOL ) _UpdateList
                 postNotification: ( BOOL ) _Post;

@end // TEEncodingManager

////////////////////////////////////////////////////////////////////////////

/****************************************************************************
 **                                                                        **
 **      _________                                      _______            **
 **     |___   ___|                                   / ______ \           **
 **         | |     _______   _______   _______      | /      |_|          **
 **         | |    ||     || ||     || ||     ||     | |    _ __           **
 **         | |    ||     || ||     || ||     ||     | |   |__  \          **
 **         | |    ||     || ||     || ||     ||     | \_ _ __| |  _       **
 **         |_|    ||_____|| ||     || ||_____||      \________/  |_|      **
 **                                           ||                           **
 **                                    ||_____||                           **
 **                                                                        **
 ***************************************************************************/
///:~