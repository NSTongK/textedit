///:
/*****************************************************************************
 **                                                                         **
 **                               .======.                                  **
 **                               | INRI |                                  **
 **                               |      |                                  **
 **                               |      |                                  **
 **                      .========'      '========.                         **
 **                      |   _      xxxx      _   |                         **
 **                      |  /_;-.__ / _\  _.-;_\  |                         **
 **                      |     `-._`'`_/'`.-'     |                         **
 **                      '========.`\   /`========'                         **
 **                               | |  / |                                  **
 **                               |/-.(  |                                  **
 **                               |\_._\ |                                  **
 **                               | \ \`;|                                  **
 **                               |  > |/|                                  **
 **                               | / // |                                  **
 **                               | |//  |                                  **
 **                               | \(\  |                                  **
 **                               |  ``  |                                  **
 **                               |      |                                  **
 **                               |      |                                  **
 **                               |      |                                  **
 **                               |      |                                  **
 **                   \\    _  _\\| \//  |//_   _ \// _                     **
 **                  ^ `^`^ ^`` `^ ^` ``^^`  `^^` `^ `^                     **
 **                                                                         **
 **                       Copyright (c) 2014 Tong G.                        **
 **                          ALL RIGHTS RESERVED.                           **
 **                                                                         **
 ****************************************************************************/

#import <Cocoa/Cocoa.h>

#import "TEDocument.h"

/* An instance of this subclass is created in the main nib file. */

// NSDocumentController is subclassed to provide for modification of the open panel.
// Normally, there is no need to subclass the document controller.
@interface TEDocumentController : NSDocumentController

// Mapping of document URLs to encoding, ignore HTML,
// and ignore rich text settings that override the defaults from Preferences
@property ( retain ) NSMutableDictionary* _customOpenSettings;

@property ( retain ) NSMutableArray* _deferredDocuments;
@property ( retain ) NSLock* _transientDocumentLock;
@property ( retain ) NSLock* _displayDocumentLock;

+ ( NSView* ) encodingAccessory: ( NSStringEncoding ) _Encoding
            includeDefaultEntry: ( BOOL ) _IncludeDefaultItem
                  encodingPopup: ( NSPopUpButton** ) _Popup
                       checkBox: ( NSButton** ) _Button;

- ( TEDocument* ) openDocumentWithContentsOfPasteboard: ( NSPasteboard* ) _Pasteboard
                                               display: ( BOOL ) _IsDisplay
                                                 error: ( out NSError** ) _OutError;

- ( NSStringEncoding ) lastSelectedEncodingForURL: ( NSURL* ) _URL;
- ( BOOL ) lastSelectedIgnoreHTMLForURL: ( NSURL* ) _URL;
- ( BOOL ) lastSelectedIgnoreRichForURL: ( NSURL* ) _URL;

- ( void ) beginOpenPanel: ( NSOpenPanel* ) _OpenPanel
                 forTypes: ( NSArray* ) _Types
        completionHandler: ( void (^)( NSInteger _Result ) ) _CompletionHandler;

- ( TEDocument* ) transientDocumentToReplace;
- ( void ) displayDocument: ( NSDocument* ) _Document;
- ( void ) replaceTransientDocument: ( NSArray* ) _Documents;

@end // TEDocumentController

////////////////////////////////////////////////////////////////////////////

/****************************************************************************
 **                                                                        **
 **      _________                                      _______            **
 **     |___   ___|                                   / ______ \           **
 **         | |     _______   _______   _______      | /      |_|          **
 **         | |    ||     || ||     || ||     ||     | |    _ __           **
 **         | |    ||     || ||     || ||     ||     | |   |__  \          **
 **         | |    ||     || ||     || ||     ||     | \_ _ __| |  _       **
 **         |_|    ||_____|| ||     || ||_____||      \________/  |_|      **
 **                                           ||                           **
 **                                    ||_____||                           **
 **                                                                        **
 ***************************************************************************/
///:~