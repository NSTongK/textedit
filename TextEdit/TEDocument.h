///:
/*****************************************************************************
 **                                                                         **
 **                               .======.                                  **
 **                               | INRI |                                  **
 **                               |      |                                  **
 **                               |      |                                  **
 **                      .========'      '========.                         **
 **                      |   _      xxxx      _   |                         **
 **                      |  /_;-.__ / _\  _.-;_\  |                         **
 **                      |     `-._`'`_/'`.-'     |                         **
 **                      '========.`\   /`========'                         **
 **                               | |  / |                                  **
 **                               |/-.(  |                                  **
 **                               |\_._\ |                                  **
 **                               | \ \`;|                                  **
 **                               |  > |/|                                  **
 **                               | / // |                                  **
 **                               | |//  |                                  **
 **                               | \(\  |                                  **
 **                               |  ``  |                                  **
 **                               |      |                                  **
 **                               |      |                                  **
 **                               |      |                                  **
 **                               |      |                                  **
 **                   \\    _  _\\| \//  |//_   _ \// _                     **
 **                  ^ `^`^ ^`` `^ ^` ``^^`  `^^` `^ `^                     **
 **                                                                         **
 **                       Copyright (c) 2014 Tong G.                        **
 **                          ALL RIGHTS RESERVED.                           **
 **                                                                         **
 ****************************************************************************/

#import <Cocoa/Cocoa.h>

@interface TEDocument : NSDocument

#pragma mark -
#pragma mark Book-keeping
@property ( assign ) BOOL _setUpPrintInfoDefault; /* YES the first time -printInfo is called */
@property ( assign ) BOOL _isInDuplicate;

#pragma mark -
#pragma mark Document Data
@property ( retain ) NSTextStorage* _textStorage; /* The (styled) text content of the document */
@property ( assign ) CGFloat _scaleFactor;		  /* The scale factor retreived from file */
@property ( assign ) BOOL _isReadOnly;            /* The document is locked and should not be modified */
@property ( retain ) NSColor* _backgroundColor;   /* The color of the document's background */
@property ( assign ) CGFloat _hyphenationFactor;  /* Hyphenation factor in range 0.0-1.0 */
@property ( assign ) NSSize _viewSize;            /* The view size, as stored in an RTF document. Can be NSZeroSize */
@property ( assign ) BOOL _hasMultiplePages;      /* Whether the document prefers a paged display */
@property ( assign ) BOOL _isUsesScreenFonts;       /* The document allows using screen fonts */

#pragma mark -
#pragma mark Document properties (applicable only to rich text documents)
// The next seven are document properties
@property ( copy ) NSString* _author;             /* Corresponds to NSAuthorDocumentAttribute */
@property ( copy ) NSString* _copyright;          /* Corresponds to NSCopyrightDocumentAttribute */
@property ( copy ) NSString* _company;            /* Corresponds to NSCompanyDocumentAttribute */
@property ( copy ) NSString* _title;              /* Corresponds to NSTitleDocumentAttribute */
@property ( copy ) NSString* _subject;            /* Corresponds to NSSubjectDocumentAttribute */
@property ( copy ) NSString* _comment;            /* Corresponds to NSCommentDocumentAttribute */
@property ( retain ) NSArray* _keywords;          /* Corresponds to NSKeywordsDocumentAttribute */

#pragma mark -
#pragma mark Information about how the document was created
@property ( assign ) BOOL _isOpenedIgnoringRighText;          /* Setting at the time the doc was open (so revert does the same thing) */
@property ( assign ) NSStringEncoding _documentEncoding;    /* NSStringEncoding used to interpret/save the document */
@property ( assign ) BOOL _isConvertedDocument;               /* Converted (or filtered) from some other format (and hence not writable) */
@property ( assign ) BOOL _isLossyDocument;                   /* Loaded lossily, so might not be a good idead to overwrite */
@property ( assign ) BOOL _isTransient;                       /* Untitled document automatically opened and never modified */
@property ( retain ) NSArray* _originalOrientationSections; /* An array of dictinoaries. Each describing the text layout orientation for a page */

#pragma mark -
#pragma mark Temporary information about how to save the document
@property ( assign ) NSStringEncoding _documentEncodingForSaving; /* NSStringEncoding for saving the document */
@property ( assign ) NSSaveOperationType _currentSaveOperation;   /* So we can know whether to use documentEncodingForSaving or documentEncoding in -fileWrapperOfType:error: */

#pragma mark -
#pragma mark Temporary information about document's desired file type 
@property ( copy ) NSString* _fileTypeToSet;    /* Actual file type determined during a read, and set after the read (which includes revert) is complete. */

#pragma mark -
#pragma mark Reading method(s)
- ( BOOL ) readFromURL: ( NSURL* ) _AbsoluteURL
                ofType: ( NSString* ) _TypeName
              encoding: ( NSStringEncoding ) _Encoding
             ignoreRTF: ( BOOL ) _IsIgnoreRTF
            ignoreHTML: ( BOOL ) _IsIgnoreHTML
                 error: ( NSError** ) _OutError;

#pragma mark -
#pragma mark IBActions
- ( IBAction ) toggleReadOnly: ( id ) _Sender;
- ( IBAction ) togglePageBreaks: ( id ) _Sender;

#pragma mark -
#pragma mark Misc
/* Is the document rich? */
- ( BOOL ) isRichText;

/* Whether conversion to rich/plain be done without loss of information */
- ( BOOL ) toggleRichWillLoseInformation;

/* Default text attributes for plain or rich text formats */
- ( NSDictionary* ) defaultTextAttributes: ( BOOL ) _ForRichText;
- ( void ) applyDefaultTextAttributes: ( BOOL ) _ForRichText;

/* Document properties */
- ( NSDictionary* ) documentPropertyToAttributeNameMappings;
- ( NSArray* ) knownDocumentProperties;
- ( void ) clearDocumentProperties;
- ( void ) setDocumentPropertiesToDefaults;
- ( BOOL ) hasDocumentProperties;

/* Page-oriented methods */
- ( NSSize ) paperSize;
- ( void ) setPaperSize: ( NSSize ) _Size;

/* Transient documents */
- ( BOOL ) isTransientAndCanBeReplaces;

@end // TEDocument

////////////////////////////////////////////////////////////////////////////

/****************************************************************************
 **                                                                        **
 **      _________                                      _______            **
 **     |___   ___|                                   / ______ \           **
 **         | |     _______   _______   _______      | /      |_|          **
 **         | |    ||     || ||     || ||     ||     | |    _ __           **
 **         | |    ||     || ||     || ||     ||     | |   |__  \          **
 **         | |    ||     || ||     || ||     ||     | \_ _ __| |  _       **
 **         |_|    ||_____|| ||     || ||_____||      \________/  |_|      **
 **                                           ||                           **
 **                                    ||_____||                           **
 **                                                                        **
 ***************************************************************************/
///:~
