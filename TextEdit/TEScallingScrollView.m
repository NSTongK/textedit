///:
/*****************************************************************************
 **                                                                         **
 **                               .======.                                  **
 **                               | INRI |                                  **
 **                               |      |                                  **
 **                               |      |                                  **
 **                      .========'      '========.                         **
 **                      |   _      xxxx      _   |                         **
 **                      |  /_;-.__ / _\  _.-;_\  |                         **
 **                      |     `-._`'`_/'`.-'     |                         **
 **                      '========.`\   /`========'                         **
 **                               | |  / |                                  **
 **                               |/-.(  |                                  **
 **                               |\_._\ |                                  **
 **                               | \ \`;|                                  **
 **                               |  > |/|                                  **
 **                               | / // |                                  **
 **                               | |//  |                                  **
 **                               | \(\  |                                  **
 **                               |  ``  |                                  **
 **                               |      |                                  **
 **                               |      |                                  **
 **                               |      |                                  **
 **                               |      |                                  **
 **                   \\    _  _\\| \//  |//_   _ \// _                     **
 **                  ^ `^`^ ^`` `^ ^` ``^^`  `^^` `^ `^                     **
 **                                                                         **
 **                       Copyright (c) 2014 Tong G.                        **
 **                          ALL RIGHTS RESERVED.                           **
 **                                                                         **
 ****************************************************************************/

#import "TEScallingScrollView.h"

#pragma mark -
#pragma mark TEScallingScrollView
@implementation TEScallingScrollView

- ( void ) awakeFromNib
    {
    [ super awakeFromNib ];

    [ self setAllowsMagnification: YES ];
    [ self setMaxMagnification: 16.f ];
    [ self setMinMagnification: 0.25 ];
    }

- ( CGFloat ) scaleFactor
    {
    return [ self magnification ];
    }

- ( void ) setScaleFactor: ( CGFloat )_NewScaleFactor
    {
    [ self setMagnification: _NewScaleFactor ];
    }

- ( void ) setScaleFactor: ( CGFloat )_NewScaleFactor
              adjustPopup: ( BOOL ) _Flag
    {
    [ self setScaleFactor:_NewScaleFactor ];
    }

#pragma mark -
#pragma mark IBAction methods
- ( IBAction ) zoomToActualSize: ( id )_Sender
    {
    [ [ self animator ] setMagnification: 1.f ];
    }

- ( IBAction ) zoomIn: ( id )_Sender
    {
    CGFloat scaleFactor = [ self scaleFactor ];
    scaleFactor = (scaleFactor > .4 && scaleFactor < .6) ? 1.f : (scaleFactor * 2.0);

    [ [ self animator ] setMagnification: scaleFactor ];
    }

- ( IBAction ) zoomOut: ( id )_Sender
    {
    CGFloat scaleFactor = [ self scaleFactor ];
    scaleFactor = (scaleFactor > 1.8f && scaleFactor < 2.2f) ? 1.f : (scaleFactor / 2.0);

    [ [ self animator ] setMagnification: scaleFactor ];
    }

- ( BOOL ) preservesContentDuringLiveResize
    {
    return [ self drawsBackground ];
    }

@end // TEScallingScrollView

////////////////////////////////////////////////////////////////////////////

/****************************************************************************
 **                                                                        **
 **      _________                                      _______            **
 **     |___   ___|                                   / ______ \           **
 **         | |     _______   _______   _______      | /      |_|          **
 **         | |    ||     || ||     || ||     ||     | |    _ __           **
 **         | |    ||     || ||     || ||     ||     | |   |__  \          **
 **         | |    ||     || ||     || ||     ||     | \_ _ __| |  _       **
 **         |_|    ||_____|| ||     || ||_____||      \________/  |_|      **
 **                                           ||                           **
 **                                    ||_____||                           **
 **                                                                        **
 ***************************************************************************/
///:~