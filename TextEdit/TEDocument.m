///:
/*****************************************************************************
 **                                                                         **
 **                               .======.                                  **
 **                               | INRI |                                  **
 **                               |      |                                  **
 **                               |      |                                  **
 **                      .========'      '========.                         **
 **                      |   _      xxxx      _   |                         **
 **                      |  /_;-.__ / _\  _.-;_\  |                         **
 **                      |     `-._`'`_/'`.-'     |                         **
 **                      '========.`\   /`========'                         **
 **                               | |  / |                                  **
 **                               |/-.(  |                                  **
 **                               |\_._\ |                                  **
 **                               | \ \`;|                                  **
 **                               |  > |/|                                  **
 **                               | / // |                                  **
 **                               | |//  |                                  **
 **                               | \(\  |                                  **
 **                               |  ``  |                                  **
 **                               |      |                                  **
 **                               |      |                                  **
 **                               |      |                                  **
 **                               |      |                                  **
 **                   \\    _  _\\| \//  |//_   _ \// _                     **
 **                  ^ `^`^ ^`` `^ ^` ``^^`  `^^` `^ `^                     **
 **                                                                         **
 **                       Copyright (c) 2014 Tong G.                        **
 **                          ALL RIGHTS RESERVED.                           **
 **                                                                         **
 ****************************************************************************/

#import "TECommon.h"

#import "TEDocument.h"
#import "TEDocumentController.h"
#import "TEDocumentWindowController.h"
#import "TETextEditDefaultsKeys.h"
#import "TEEncodingManager.h"
#import "TETextEditError.h"

#define kOldEditPaddingCompensation 12.0

#pragma mark -
#pragma mark UTIs
NSString static* const kSimpleTextType = @"com.apple.traditional-mac-plain-text";
NSString static* const kWord97Type = @"com.microsoft.word.doc";
NSString static* const kWord2007Type = @"org.openxmlformats.wordprocesssingml.document";
NSString static* const kWord2003XMLType = @"com.microsoft.word.wordml";
NSString static* const kOpenDocumentTextType = @"org.oasis-open.opendocument.text";

// TEDocument class
@implementation TEDocument

#pragma mark -
#pragma mark Book-keeping
@synthesize _setUpPrintInfoDefault;
@synthesize _isInDuplicate;

#pragma mark -
#pragma mark Document Data
@synthesize _textStorage;
@synthesize _scaleFactor;
@synthesize _backgroundColor;
@synthesize _hyphenationFactor;
@synthesize _viewSize;
@synthesize _hasMultiplePages;
@synthesize _isUsesScreenFonts;

#pragma mark -
#pragma mark Document properties (applicable only to rich text documents)
@synthesize _author;
@synthesize _copyright;
@synthesize _company;
@synthesize _title;
@synthesize _subject;
@synthesize _comment;
@synthesize _keywords;

#pragma mark -
#pragma mark Information about how the document was created
@synthesize _isOpenedIgnoringRighText;
@synthesize _documentEncoding;
@synthesize _isConvertedDocument;
@synthesize _isLossyDocument;
@synthesize _isTransient;
@synthesize _originalOrientationSections;

#pragma mark -
#pragma mark Temporary information about how to save the document
@synthesize _documentEncodingForSaving;
@synthesize _currentSaveOperation;

#pragma mark -
#pragma mark Temporary information about document's desired file type
@synthesize _fileTypeToSet;

+ ( BOOL ) isRichTextType: ( NSString* ) _TypeName
    {
    /* We map all plain text documents to public.text.  
     * Therefore a document is rich iff its type is not public.text. */
    return ![ _TypeName isEqualToString: ( NSString* )kUTTypeText ];
    }

+ ( NSString* ) readableTypeForType: ( NSString* ) _Type
    {
    // There is a partial order on readableTypes given by UTTypeConformsTo.
    // We linearly extend the partial order to a total order using <.
    // Therefore we can compute the ancestor with greatest level (furthest from root) by linear search in the resulting array.
    // Why do we have to do this?  Because type might conform to multiple readable types,
    // such as "public.rtf" and "public.text" and "public.data"
    // and we want to find the most specialized such type.
    NSArray static* topologicallySortedReadableTypes;
    dispatch_once_t static pred;
    dispatch_once( &pred, ^{
        topologicallySortedReadableTypes = [ self readableTypes ];

        topologicallySortedReadableTypes =
            [ topologicallySortedReadableTypes sortedArrayUsingComparator:
                ^NSComparisonResult( id _Type1, id _Type2 )
                    {
                    if ( _Type1 == _Type2 )                                                 return NSOrderedSame;
                    if ( UTTypeConformsTo( ( CFStringRef )_Type1, ( CFStringRef )_Type2 ) ) return NSOrderedAscending;
                    if ( UTTypeConformsTo( ( CFStringRef )_Type2, ( CFStringRef )_Type1 ) ) return NSOrderedDescending;

                    return ( ( ( NSUInteger )_Type1 < ( NSUInteger )_Type2 ) ? NSOrderedAscending : NSOrderedDescending );
                    } ];

                [ topologicallySortedReadableTypes retain ];
            } );

    for ( NSString* readableType in topologicallySortedReadableTypes )
        if ( UTTypeConformsTo( ( CFStringRef )_Type, ( CFStringRef )readableType ) )
            return readableType;

    return nil;
    }

- ( id ) init
    {
    if ( self = [ super init ] )
        {
        [ [ self undoManager ] disableUndoRegistration ];

        self._textStorage = [ [ NSTextStorage allocWithZone: self.zone ] init ];

        [ self set_backgroundColor: [ NSColor whiteColor ] ];
        [ self set_documentEncoding: TEEncodingManagerNoStringEncoding ];
        [ self set_documentEncodingForSaving: TEEncodingManagerNoStringEncoding ];
        [ self set_scaleFactor: 1.f ];
        [ self setDocumentPropertiesToDefaults ];

        self._isInDuplicate = NO;

        // Assume the default file type for now,
        // since -initWithType:error does not currently get called when creating documents using AppleScript.
        [ self setFileType: [ [ NSDocumentController sharedDocumentController ] defaultType ] ];

        [ self setPrintInfo: [ self printInfo ] ];

        self._hasMultiplePages = [ USER_DEFAULTS boolForKey: TETextEditShowPageBreaks ];

        [ self set_usesScreenFonts: [ self isRichText ] ? [ USER_DEFAULTS boolForKey: TETextEditUseScreenFonts ] : YES ];

        [ [ self undoManager ] enableUndoRegistration ];
        }

    return self;
    }

/* Return an NSDictionary which maps Cocoa text system document identifiers 
 * (as declared in AppKit/NSAttributedString.h) to document types declared in TextEdit's Info.plist.
 */
- ( NSDictionary* ) textDocumentTypeToTextEditDocumentTypeMappingTable
    {
    static NSDictionary* documentMappings = nil;

    // Use of dispatch_once() makes the initialization thread-safe,
    // and it needs to be, since multiple documents can be opened concurrently
    static dispatch_once_t once = 0;
    dispatch_once( &once, ^{
        documentMappings = @{ NSPlainTextDocumentType         : ( NSString* )kUTTypeText
                            , NSRTFTextDocumentType           : ( NSString* )kUTTypeRTF
                            , NSRTFDTextDocumentType          : ( NSString* )kUTTypeRTFD
                            , NSMacSimpleTextDocumentType     : ( NSString* )kSimpleTextType
                            , NSHTMLTextDocumentType          : ( NSString* )kUTTypeHTML
                            , NSDocFormatTextDocumentType     : ( NSString* )kWord97Type
                            , NSOfficeOpenXMLTextDocumentType : ( NSString* )kWord2007Type
                            , NSWordMLTextDocumentType        : ( NSString* )kWord2003XMLType
                            , NSOpenDocumentTextDocumentType  : ( NSString* )kOpenDocumentTextType
                            , NSWebArchiveTextDocumentType    : ( NSString* )kUTTypeWebArchive
                            };
        } );

    return documentMappings;
    }

/* This method is called by the document controller. 
 * The message is passed on after information about the selected encoding (from our controller subclass) and preference regarding HTML and RTF formatting has been added. 
 * -lastSelectedEncodingForURL: returns the encoding specified in the Open panel,
 * or the default encoding if the document was opened without an open panel. 
 */
#pragma mark -
#pragma mark Reading method(s)
- ( BOOL ) readFromURL: ( NSURL* ) _AbsoluteURL
                ofType: ( NSString* ) _TypeName
                 error: ( NSError** ) _OutError
    {
    TEDocumentController* docController = [ TEDocumentController sharedDocumentController ];

    return [ self readFromURL: _AbsoluteURL
                       ofType: _TypeName
                     encoding: [ docController lastSelectedEncodingForURL: _AbsoluteURL ]
                    ignoreRTF: [ docController lastSelectedIgnoreRichForURL: _AbsoluteURL ]
                   ignoreHTML: [ docController lastSelectedIgnoreHTMLForURL: _AbsoluteURL ]
                        error: _OutError
                   ];
    }

- ( BOOL ) readFromURL: ( NSURL* ) _AbsoluteURL
                ofType: ( NSString* ) _TypeName
              encoding: ( NSStringEncoding ) _Encoding
             ignoreRTF: ( BOOL ) _IsIgnoreRTF
            ignoreHTML: ( BOOL ) _IsIgnoreHTML
                 error: ( NSError** ) _OutError
    {
    NSMutableDictionary* optionsDict = [ NSMutableDictionary dictionaryWithCapacity: 5 ];
    NSDictionary* docAttrs;

    id val, paperSizeVal, viewSizeVal;

    NSTextStorage* textStorage = [ self _textStorage ];

    /* generalize the passed-in type to a type we support.  
     * for instance, generalize "public.xml" to "public.txt" */
    _TypeName = [ [ self class ] readableTypeForType: _TypeName ];

    [ self._fileTypeToSet release ];
    self._fileTypeToSet = nil;

    [ [ self undoManager ] disableUndoRegistration ];

    optionsDict[ NSBaseURLDocumentOption ] = _AbsoluteURL;

    if ( _Encoding != TEEncodingManagerNoStringEncoding )
        optionsDict[ NSCharacterEncodingDocumentOption ] = [ NSNumber numberWithUnsignedInteger: _Encoding ];

    [ self set_documentEncoding: _Encoding ];

    // Check type to see if we should load the document as plain.
    // Note that this check isn't always conclusive, which is why we do another check below,
    // after the document has been loaded (and correctly categorized).
    NSWorkspace* workspace = SHARED_WORKSPACE;
    // TODO: Replace the NSWorkspace method type:conformsToType: with UTConformsType() function.
    if ( ( _IsIgnoreRTF
                && ( [ workspace type: _TypeName conformsToType: ( NSString* )kUTTypeRTF ]
                        || [ workspace type: _TypeName conformsToType: kWord2003XMLType ] ) )
            || ( _IsIgnoreHTML && [ workspace type: _TypeName conformsToType: ( NSString* )kUTTypeHTML ] )
            || self._isOpenedIgnoringRighText )
        {
        optionsDict[ NSDocumentTypeDocumentOption ] = NSPlainTextDocumentType;
        _TypeName = ( NSString* )kUTTypeText;

        [ self set_isOpenedIgnoringRighText: YES ];
        }

    [ [ textStorage mutableString ] setString: @"" ];

    // Remove the layout managers while loading the text;
    // mutableCopy retains the array so the layout managers aren't released
    NSMutableArray* layoutMgrs = [ [ textStorage layoutManagers ] mutableCopy ];
    NSEnumerator* layoutMgrEnum = [ layoutMgrs objectEnumerator ];
    NSLayoutManager* layoutMgr = nil;
    while ( ( layoutMgr = [ layoutMgrEnum nextObject ] ) )
        [ textStorage removeLayoutManager: layoutMgr ];

    // We can do this loop twice,
    // if the document is loaded as rich text although the user requested plain
    BOOL retry;

    do {
        BOOL success;
        NSString* docType;

        retry = NO;

        [ textStorage beginEditing ];
        success = [ textStorage readFromURL: _AbsoluteURL
                                    options: optionsDict
                         documentAttributes: &docAttrs
                                      error: _OutError ];
        if ( !success )
            {
            [ textStorage endEditing ];

            layoutMgrEnum = [ layoutMgrs objectEnumerator ]; // rewind

            while ( ( layoutMgr = [ layoutMgrEnum nextObject ] ) )
                [ textStorage addLayoutManager: layoutMgr ];

            [ layoutMgrs release ];

            return NO;  // return NO on error; _OutError has already been set
            }

        docType = docAttrs[ NSDocumentTypeDocumentAttribute ];

    	// First check to see if the document was rich and should have been loaded as plain
        if ( ![ optionsDict[ NSDocumentTypeDocumentOption ] isEqualToString: NSPlainTextDocumentType ]
                && ( ( _IsIgnoreHTML && [ docType isEqualToString: NSHTMLTextDocumentType ] )
                        || ( _IsIgnoreRTF && ( [ docType isEqualToString: NSRTFTextDocumentType ] || [ docType isEqualToString: NSWordMLTextDocumentType ] ) ) ) )
            {
            [ textStorage endEditing ];
            [ [ textStorage mutableString ] setString: @"" ];

            optionsDict[ NSDocumentTypeDocumentOption ] = NSPlainTextDocumentType;
            _TypeName = ( NSString* )kUTTypeText;

            [ self set_isOpenedIgnoringRighText: YES ];

            retry = YES;
            }
        else
            {
            NSString* newFileType = [ [ self textDocumentTypeToTextEditDocumentTypeMappingTable ] objectForKey: docType ];

            if ( newFileType )
                _TypeName = newFileType;
            else
                _TypeName = ( NSString* )kUTTypeRTF; // Hmm, a new type in Cocoa text system. Treat is as rich. ??? Should set the converted flag too?
            }

        if ( ![ [ self class ] isRichTextType: _TypeName ] )
            [ self applyDefaultTextAttributes: NO ];

        [ textStorage endEditing ];
        } while ( retry );

    [ self setFileType: _TypeName ];

    // If we're reverting, NSDocument will set the file type behind out backs.
    // This enables restoring that type.
    self._fileTypeToSet = [ _TypeName copy ];

    layoutMgrEnum = [ layoutMgrs objectEnumerator ]; // rewind

    while ( ( layoutMgr = [ layoutMgrEnum nextObject ] ) )
        [ textStorage addLayoutManager: layoutMgr ];    // Add the layout managers back
    [ layoutMgrs release ];

    val = docAttrs[ NSCharacterEncodingDocumentAttribute ];
    [ self set_documentEncoding: ( val ? [ val unsignedIntegerValue ] : TEEncodingManagerNoStringEncoding ) ];

    if ( ( val = docAttrs[ NSConvertedDocumentAttribute ] ) )
        {
        [ self set_isConvertedDocument: ( [ val integerValue ] > 0 ) ]; // Indicates filtered
        [ self set_isLossyDocument: ( [ val integerValue ] < 0 ) ];     // Indicates lossily loaded

        /* If the document has a stored value for view mode, use it.
         * Otherwise wrap to window. */
        if ( ( val = docAttrs[ NSViewModeDocumentAttribute ] ) )
            {
            [ self set_hasMultiplePages: ( [ val integerValue ] == 1 ) ];

            if ( ( val = docAttrs[ NSViewZoomDocumentAttribute ] ) )
                [ self set_scaleFactor: ( [ val doubleValue ] / 100.f ) ];
            }
        else
            [ self set_hasMultiplePages: NO ];

        [ self willChangeValueForKey: @"printInfo" ];
        if ( ( val = docAttrs[ NSLeftMarginDocumentAttribute ] ) )   [ [ self printInfo ] setLeftMargin:   [ val doubleValue ] ];
        if ( ( val = docAttrs[ NSRightMarginDocumentAttribute ] ) )  [ [ self printInfo ] setRightMargin:  [ val doubleValue ] ];
        if ( ( val = docAttrs[ NSBottomMarginDocumentAttribute ] ) ) [ [ self printInfo ] setBottomMargin: [ val doubleValue ] ];
        if ( ( val = docAttrs[ NSTopMarginDocumentAttribute ] ) )    [ [ self printInfo ] setTopMargin:    [ val doubleValue ] ];
        [ self didChangeValueForKey: @"printInfo" ];

        /* Pre MacOSX versions of TextEdit wrote out the view (window) size in PaperSize.
         * If we encounter a non-MacOSX RTF file, and it's written by TextEdit, use PaperSize as ViewSize */
        viewSizeVal = docAttrs[ NSViewSizeDocumentAttribute ];
        paperSizeVal = docAttrs[ NSPaperSizeDocumentAttribute ];
        if ( paperSizeVal && NSEqualSizes( [ paperSizeVal sizeValue ], NSZeroSize ) )
            paperSizeVal = nil; // Protect against some old documents with 0 paper size

        if ( viewSizeVal )
            {
            [ self set_viewSize: [ viewSizeVal sizeValue ] ];

            if ( paperSizeVal )
                [ self setPaperSize: [ paperSizeVal sizeValue ] ];
            }
        else // No ViewSize...
            {
            if ( paperSizeVal ) // See if PaperSize should be used as ViewSize; if so, we also have some tweaking to do on it
                {
                val = docAttrs [ NSCocoaVersionDocumentAttribute ];
                if ( val && ( [ val integerValue ] < 100 ) ) // Indicates old RTF file; value described in AppKit/NSAttributedString.h
                    {
                    NSSize size = [ paperSizeVal sizeValue ];
                    if ( size.width > 0 && size.height > 0 && ![ self _hasMultiplePages ] )
                        {
                        size.width = size.width - kOldEditPaddingCompensation;
                        [ self set_viewSize: size ];
                        }
                    }
                else
                    [ self setPaperSize: [ paperSizeVal sizeValue ] ];
                }
            }
        }

    [ self set_hyphenationFactor: ( val = docAttrs[ NSHyphenationFactorDocumentAttribute ] ) ? [ val floatValue ] : 0 ];
    [ self set_backgroundColor: ( val = docAttrs[ NSBackgroundColorAttributeName ] ) ? val : [ NSColor whiteColor ] ];

    // Set the document properties, generically, going through key value coding
    NSDictionary* map = [ self documentPropertyToAttributeNameMappings ];
    for ( NSString* property in [ self knownDocumentProperties ] )
        [ self setValue: [ docAttrs objectForKey: [ map objectForKey: property ] ] forKey: property ];

    [ self set_isReadOnly: ( ( val = docAttrs[ NSReadOnlyDocumentAttribute ] ) && ( [ val integerValue ] > 0 ) ) ];

    [ self set_originalOrientationSections: [ docAttrs objectForKey: NSTextLayoutSectionOrientation ] ];

    [ self set_isUsesScreenFonts: [ self isRichText ] ? [ docAttrs[ NSUsesScreenFontsDocumentAttribute ] boolValue ] : YES ];

    [ [ self undoManager ] enableUndoRegistration ];

    return YES;
    }

#pragma mark -
#pragma mark Misc
/* Is the document rich? */
- ( BOOL ) isRichText
    {
    return [ [ self class ] isRichTextType: self.fileType ];
    }

/* Default text attributes for plain or rich text formats */
- ( NSDictionary* ) defaultTextAttributes: ( BOOL ) _ForRichText
    {
    static NSParagraphStyle* defaultRichParaStyle = nil;
    NSMutableDictionary* textAttributes = [ [ [ NSMutableDictionary alloc ] initWithCapacity: 2 ] autorelease ];

    if ( _ForRichText )
        {
        textAttributes[ NSFontAttributeName ] = [ NSFont userFontOfSize: 0.0 ];

        if ( !defaultRichParaStyle ) // We do this once...
            {
            NSString* measurementUnits = [ USER_DEFAULTS objectForKey: TETextEidtAppleMeasurementUnits ];
            CGFloat tabInterval = ( [ @"Centimeters" isEqualToString: measurementUnits ] ) ? (72.0 / 2.54) : (72.0 / 2.0); // Every cm or half inch

            NSMutableParagraphStyle* paraStyle = [ [ [ NSMutableParagraphStyle alloc ] init ] autorelease ];
            NSTextTabType type = ( ( [ NSParagraphStyle defaultWritingDirectionForLanguage: nil ] ) ? NSRightTabStopType : NSLeftTabStopType );

            [ paraStyle setTabStops: [ NSArray array ] ];   // This first clears all tab stops

            for ( NSInteger cnt = 0; cnt < 12; cnt++ )  // Add 12 tab stops, at desired intervals...
                {
                NSTextTab* tabShop = [ [ [ NSTextTab alloc ] initWithType: type location: tabInterval * ( cnt + 1 ) ] autorelease ];

                [ paraStyle addTabStop: tabShop ];
                }

            defaultRichParaStyle = [ paraStyle copy ];
            }

        textAttributes[ NSParagraphStyleAttributeName ] = defaultRichParaStyle;
        }
    else
        {
        NSFont* plainFont = [ NSFont userFixedPitchFontOfSize: .0f ];
        NSFont* charWidthFont = [ plainFont screenFontWithRenderingMode: NSFontDefaultRenderingMode ];

        NSInteger tabWith = [ USER_DEFAULTS integerForKey: TETextEditTabWidth ];
        CGFloat charWidth = [ @" " sizeWithAttributes: @{ NSFontAttributeName : charWidthFont } ].width;

        if ( charWidth == 0 )
            charWidth = [ charWidthFont maximumAdvancement ].width;

        // Now use a default paragraph style, but with the tab width adjusted
        NSMutableParagraphStyle* mutableStyle = [ [ [ NSParagraphStyle defaultParagraphStyle ] mutableCopy ] autorelease ];
        [ mutableStyle setTabStops: [ NSArray array ] ];
        [ mutableStyle setDefaultTabInterval: ( charWidth * tabWith ) ];

        textAttributes[ NSParagraphStyleAttributeName ] = [ [ mutableStyle copy ] autorelease ];

        // Also set the font
        textAttributes[ NSFontAttributeName ] = plainFont;
        }

    return textAttributes;
    }

- ( void ) applyDefaultTextAttributes: ( BOOL ) _ForRichText
    {
    NSDictionary* textAttributes = [ self defaultTextAttributes: _ForRichText ];
    NSTextStorage* textStorage = [ self _textStorage ];

    // We now preserve base writing direction even for plain text,
    // using the 10.6-introduced attribute enumeration API
    [ textStorage enumerateAttribute: NSParagraphStyleAttributeName
                             inRange: NSMakeRange( 0, [ textStorage length ] )
                             options: 0
                          usingBlock:
        ^( id _ParagraphStyle, NSRange _ParagraphStyleRange, BOOL* _Stop )
            {
            NSWritingDirection writingDirection =
                _ParagraphStyle ? [ (NSParagraphStyle*)_ParagraphStyle baseWritingDirection ] : NSWritingDirectionNatural;

            // We also preserve NSWritingDirectionAttributeName (new in 10.6)
            [ textStorage enumerateAttribute: NSWritingDirectionAttributeName
                                     inRange: _ParagraphStyleRange
                                     options: 0
                                  usingBlock:
                ^( id _Value, NSRange attributeRange, BOOL* _Stop )
                    {
                    [ _Value retain ];
                    [ textStorage setAttributes: textAttributes range: attributeRange ];

                    if ( _Value )
                        [ textStorage addAttribute: NSWritingDirectionAttributeName value: _Value range: attributeRange ];

                    [ _Value release ];
                    } ];

            if ( writingDirection != NSWritingDirectionNatural )
                [ textStorage setBaseWritingDirection: writingDirection range: _ParagraphStyleRange ];
            } ];
    }

/* This method will return a suggested encoding for the document. 
 * since Mac OS X Leopard, unless the user has specified a favorite
 * encoding for saving that applies to document, we use UTF-8.
 */
- ( NSStringEncoding ) suggestedDocumentEncoding
    {
    NSUInteger encoding = TEEncodingManagerNoStringEncoding;
    NSNumber* val = [ USER_DEFAULTS objectForKey: TETextEditPlainTextEncodingForWrite ];

    if ( val )
        {
        NSStringEncoding chosenEncoding = [ val unsignedIntegerValue ];

        if ( ( chosenEncoding != TEEncodingManagerNoStringEncoding )
            && ( chosenEncoding != NSUnicodeStringEncoding )
            && ( chosenEncoding != NSUTF8StringEncoding ) )
            if ( [ [ self._textStorage string ] canBeConvertedToEncoding: chosenEncoding ] )
                encoding = chosenEncoding;
        }

    if ( encoding == TEEncodingManagerNoStringEncoding )
        encoding = NSUTF8StringEncoding;    // Default to UTF-8

    return encoding;
    }

/* Document properties */

/* Table mapping document property keys "company", etc,
 * to text system document attribute keys ( NSCompanyDocumentAttribute, etc )
 */
- ( NSDictionary* ) documentPropertyToAttributeNameMappings
    {
    static NSDictionary* dict = nil;
    static dispatch_once_t onceToken;

    dispatch_once( &onceToken, ^{
        dict = @{ @"company"    : NSCompanyDocumentAttribute
                , @"author"     : NSAuthorDocumentAttribute
                , @"keywords"   : NSKeywordsDocumentAttribute
                , @"copyright"  : NSCopyrightDocumentAttribute
                , @"title"      : NSTitleDocumentAttribute
                , @"subject"    : NSSubjectDocumentAttribute
                , @"comment"    : NSCommentDocumentAttribute
                }; } );

    return dict;
    }

- ( NSArray* ) knownDocumentProperties
    {
    return [ [ self documentPropertyToAttributeNameMappings ] allKeys ];
    }

- ( void ) clearDocumentProperties
    {

    }

- ( void ) setDocumentPropertiesToDefaults
    {

    }

- ( BOOL ) hasDocumentProperties
    {
    // TODO:
    return NO;
    }

/* Page-oriented methods */
- ( NSSize ) paperSize
    {
    return [ [ self printInfo ] paperSize ];
    }

- ( void ) setPaperSize: ( NSSize ) _Size
    {
    NSPrintInfo* oldPrintInfo = [ self printInfo ];

    if ( !NSEqualSizes( _Size, [ oldPrintInfo paperSize ] ) )
        {
        NSPrintInfo* newPrintInfo = [ oldPrintInfo copy ];

        [ newPrintInfo setPaperSize: _Size ];
        [ self setPrintInfo: newPrintInfo ];
        [ newPrintInfo release ];
        }
    }

/* Clear the delegates of the text views and window,
 * then release all resources and go away...
 */
- ( void ) dealloc
    {
    [ self._textStorage release ];
    [ self._backgroundColor release ];

    [ self._author release ];
    [ self._comment release ];
    [ self._subject release ];
    [ self._title release ];
    [ self._keywords release ];
    [ self._copyright release ];
    [ self._company release ];

    [ self._fileTypeToSet release ];

    [ self._originalOrientationSections release ];

    [ super dealloc ];
    }

- ( BOOL ) isTransientAndCanBeReplaces
    {
    if ( ![ self _isTransient ] )
        return NO;

    for ( NSWindowController* controller in [ self windowControllers ] )
        if ( [ [ controller window ] attachedSheet ] )
            return NO;

    return YES;
    }

- ( void ) setFileType: ( NSString* ) _Type
    {
    /* Due to sandboxing, we cannot (usefully) directly change the document's file URL except for changing it to nil.  
     * This means that when we convert a document from rich text to plain text, 
     * our only way of updating the file URL is to have NSDocument do it for us in response to a change in our file type. 
     * However, it is not as simple as setting our type to kUTTypeText, as would be accurate, 
     * because if we have a rtf document, public.rtf inherits from public.text and 
     * so NSDocument wouldn't change our extension (which is correct, BTW,
     * since it's also perfectly valid to open a rtf and not interpret the rtf commands, 
     * in which case a save of a rtf document as kUTTypeText should not change the extension). 
     * Therefore we need to save using a subtype of kUTTypeText that isn't in the path from kUTTypeText to kUTTypeRTF. 
     * The obvious candidate is kUTTypePlainText. 
     * Therefore, we need to save using kUTTypePlainText when we convert a rtf to plain text and then map the file type from kTTypePlainText to kUTTypeText.
     * The inverse of the mapping occurs here. */
    if ( UTTypeEqual( ( CFStringRef )_Type, kUTTypePlainText ) )
        _Type = ( NSString* )kUTTypeText;

    [ super setFileType: _Type ];
    }

- ( IBACTION_BUT_NOT_FOR_IB ) appendPlainTextExtensionChanged: ( id ) _Sender
    {
    NSSavePanel* panel = (NSSavePanel*)[ _Sender window ];
    [ panel setAllowsOtherFileTypes: [ _Sender state ] ];
    [ panel setAllowedFileTypes: [ _Sender state ] ? @[ (NSString*)kUTTypePlainText ] : nil ];
    }

- ( IBACTION_BUT_NOT_FOR_IB ) encodingPopupChanged: ( NSPopUpButton* ) _Popup
    {
    [ self set_documentEncodingForSaving: [ [ [ _Popup selectedItem ] representedObject ] unsignedIntegerValue ] ];
    }

@end // TEDocument

// TEDocument + TETextEditDocumentOverrides
@implementation TEDocument ( TETextEditDocumentOverrides )

+ (BOOL)autosavesInPlace
{
    return YES;
}

+ ( BOOL ) canConcurrentlyReadDocumentsOfType: ( NSString* ) _TypeName
    {
    return !( UTTypeConformsTo( (CFStringRef)_TypeName, kUTTypeHTML )
                || UTTypeConformsTo( (CFStringRef)_TypeName, kUTTypeWebArchive ) );
    }

- ( void ) makeWindowControllers
    {
    NSArray* controllers = [ self windowControllers ];

    // If this document displace a transient document,
    // it will already have been assigned a window controller.
    // If that is not the case, create one.
    if ( [ controllers count ] == 0 )
        [ self addWindowController: [ [ [ TEDocumentWindowController alloc ] init ] autorelease ] ];
    }

/* One of the determinants of whether a file is locked is whether its type is one of our writable types.
 * However, the writable types are a function of whether the document contains attachments.
 * But whether we are locked cannot be a function of whether the document contains attachments, 
 * because we won't be asked to redetermine autosaving safety after an undo operation resulting from a cancel, 
 * so the document would continue to appear locked if an image were dragged in and then cancel was pressed.  
 * Therefore, we must use an "ignoreTemporary" boolean to treat RTF as temporarily writable despite the attachments.  
 * That's fine since -checkAutosavingSafetyAfterChangeAndReturnError:
 * will perform this check again with ignoreTemporary set to NO, 
 * and that method will be called when the operation is done and undone, 
 * so no inconsistency results.
 */
- ( NSArray* ) writableTypesForSaveOperation: ( NSSaveOperationType ) _SaveOperation
                        ignoreTemporaryState: ( BOOL ) _IgnoreTemporary
    {
    NSMutableArray* outArray = [ [ [ self class ] writableTypes ] mutableCopy ];

    if ( _SaveOperation == NSSaveAsOperation )
        {
        // Rich-text documents cannot be saved as plain text.
        if ( [ self isRichText ] )
            {
            [ outArray removeObject: (NSString*)kUTTypeText ];
            [ outArray removeObject: (NSString*)kUTTypePlainText ];
            }

        // Documents that contain attachments can only be saved in formats that support embedded graphics.
        if ( !_IgnoreTemporary && [ self._textStorage containsAttachments ] )
            [ outArray setArray: @[ (NSString*)kUTTypeRTFD, (NSString*)kUTTypeWebArchive ] ];
        }

    return [ outArray autorelease ];
    }

- ( NSArray* ) writableTypesForSaveOperation: ( NSSaveOperationType ) _SaveOperation
    {
    return [ self writableTypesForSaveOperation: _SaveOperation ignoreTemporaryState: NO ];
    }

- ( NSString* ) fileNameExtensionForType: ( NSString* ) _InTypeName
                           saveOperation: ( NSSaveOperationType ) _InSaveOperation
    {
    /* We use kUTTypeText as our plain text type.  
     * However, kUTTypeText is really a class of types and therefore contains no preferred extension.
     * Therefore we must specify a preferred extension, that of kUTTypePlainText. */
    if ( UTTypeEqual( (CFStringRef)_InTypeName, kUTTypeText ) )
        return @"txt";

    return [ super fileNameExtensionForType: _InTypeName saveOperation: _InSaveOperation ];
    }

/* When we save, we send a notification so that views that are currently coalescing undo actions can break that. This is done for two reasons, one technical and the other HI oriented. 

Firstly, since the dirty state tracking is based on undo, for a coalesced set of changes that span over a save operation, the changes that occur between the save and the next time the undo coalescing stops will not mark the document as dirty. Secondly, allowing the user to undo back to the precise point of a save is good UI. 

In addition we overwrite this method as a way to tell that the document has been saved successfully. If so, we set the save time parameters in the document.
*/
- ( void )  saveToURL: ( NSURL* ) _AbsoluteURL
               ofType: ( NSString* ) _TypeName
     forSaveOperation: ( NSSaveOperationType ) _SaveOperation
    completionHandler: ( void (^)( NSError* error ) ) _Handler
    {
    // Note that we do the breakUndoCoalescing call even during autosave,
    // which means the user's undo of long typing will take them back to the last spot an autosave occured.
    // This might seem confusing, and a more elaborate solution may be possible
    // (cause an autosave without having to breakUndoCoalescing),
    // but since this change is coming late in Leopard,
    // we decided to go with the lower risk fix.
    [ [ self windowControllers ] makeObjectsPerformSelector: @selector( breakUndoCoalescing ) ];
    [ self performAsynchronousFileAccessUsingBlock:
        ^( void ( ^_FileAccessCompletionHandler )( void ) )
            {
            self._currentSaveOperation = _SaveOperation;

            [ super saveToURL:_AbsoluteURL ofType:_TypeName forSaveOperation:_SaveOperation
            completionHandler: ^( NSError* _ErrorOrNil )
                {
                [ self set_documentEncodingForSaving: TEEncodingManagerNoStringEncoding ];
                _FileAccessCompletionHandler();

                _Handler( _ErrorOrNil );
                } ];
            } ];
    }

/* Indicate the types we know we can save safely asynchronously. */
- ( BOOL ) canAsynchronouslyWriteToURL: ( NSURL* ) _URL
                                ofType: ( NSString* ) _TypeName
                      forSaveOperation: ( NSSaveOperationType ) _SaveOperation
    {
    return [ [ self class ] canConcurrentlyReadDocumentsOfType: _TypeName ];
    }

- ( NSError* ) errorInTextEditDomainWithCode: ( NSInteger ) _ErrorCode
    {
    switch ( _ErrorCode )
        {
    case TETextEditSaveErrorWritableTypeRequired:
            {
            NSString* description = nil;
            NSString* recoverySuggestion = nil;

            /* the document can't be saved in its original format, 
             * either because TextEdit cannot write to the format, 
             * or TextEdit cannot write documents containing
             * attachments to the format. */
            if ( [ self._textStorage containsAttachments ] )
                {
                description = NSLocalizedString( @"Convert this document to RTFD format?"
                                               , @"Title of alert panel prompting the user to convert to RTFD." );
                recoverySuggestion = NSLocalizedString( @"Document with graphics and attachments will be saved using RTFD (RTF with graphics) format. RTFD documents are not compatible with some applications. Convert anyway?"
                                                      , @"Contents of alert panel prompting the user to convert to RTFD." );
                }
            else
                {
                description = NSLocalizedString( @"Convert this document to RTF?"
                                               , @"Title of alert panel prompting the user to convert to RTF" );
                recoverySuggestion = NSLocalizedString( @"This document must be converted to RTF before it can be modified."
                                                      , @"Contents of alert panel prompting the user to convert to RTF." );
                }

            NSError* error = [ NSError errorWithDomain: TETextEditErrorDomain
                                                  code: TETextEditSaveErrorWritableTypeRequired
                                              userInfo: @{ NSLocalizedDescriptionKey : description
                                                         , NSLocalizedRecoverySuggestionErrorKey : recoverySuggestion
                                                         , NSLocalizedRecoveryOptionsErrorKey :
                                                                @[ NSLocalizedString( @"Convert",   @"Button choice that allows the user to convert the document." )
                                                                 , NSLocalizedString( @"Cancel",    @"Button choice that allows the user to cancel." )
                                                                 , NSLocalizedString( @"Duplicate", @"Button choise that allows the user to duplicate." )
                                                                 ]

                                                         , NSRecoveryAttempterErrorKey: self } ];
            return error;
            }

    case TETextEditSaveErrorConvertedDocument:
            {
            NSString* newFormatName = [ self._textStorage containsAttachments ]
                                            ? NSLocalizedString( @"rich text with graphics (RTFD)", @"Rich text with graphics file format name, displayed in alert" )
                                            : NSLocalizedString( @"rich text", @"Rich text file format name, displayed in alert" );
            return [ NSError errorWithDomain: TETextEditErrorDomain
                                        code: TETextEditSaveErrorConvertedDocument
                                    userInfo: @{ NSLocalizedDescriptionKey : NSLocalizedString( @"Are you sure you want to edit this document?"
                                                                                              , @"Title of alert panel asking the suer whether he wants to edit a converted document." )
                                               , NSLocalizedRecoverySuggestionErrorKey :
                                                    [ NSString stringWithFormat: NSLocalizedString( @"This document was converted from a format that TextEdit cannot save. It will be saved in %@ format."
                                                                                                  , @"Contents of alert panel infoming user that the document is converted and cannot be written in its original format." )
                                                                               , newFormatName ]
                                               , NSLocalizedRecoveryOptionsErrorKey :
                                                    @[ NSLocalizedString( @"Edit",   @"Button choice that allows the user to save the document." )
                                                     , NSLocalizedString( @"Cancel", @"Button choice that allows the user to cancel." )
                                                     , NSLocalizedString( @"Duplicate", @"Button choice that allows the user to duplicate the documents." )
                                                     ]

                                               , NSRecoveryAttempterErrorKey : self } ];
            }

    case TETextEditSaveErrorLossyDocument:
            {
            return [ NSError errorWithDomain: TETextEditErrorDomain
                                        code: TETextEditSaveErrorLossyDocument
                                    userInfo: @{ NSLocalizedDescriptionKey : NSLocalizedString( @"Are you sure you want to modify the document in place?"
                                                                                              , @"Title of alert panel which bring up a warning about saving over the same document" )
                                               , NSLocalizedRecoverySuggestionErrorKey : NSLocalizedString( @"Modifying the document in place might cause you to lose some of the original formatting. Would you like to duplicate the document first?"
                                                                                                          , @"Contents of alert panel informing user that they need to supply a new file name because the save might be lossy" )
                                               , NSLocalizedRecoveryOptionsErrorKey
                                                    : @[ NSLocalizedString( @"Duplicate", @"Button choice that allows the suer to duplicate the document." )
                                                       , NSLocalizedString( @"Cancel", @"Button choice that allows the uer to cancel." )
                                                       , NSLocalizedString( @"Overwirte", @"Button choice allowing user to overwrite the document." )
                                                       ]

                                               , NSRecoveryAttempterErrorKey : self } ];
            }

    case TETextEditSaveErrorEncodingInapplicable:
            {
            NSUInteger encode = [ self _documentEncodingForSaving ];

            if ( encode == TEEncodingManagerNoStringEncoding )
                encode = [ self _documentEncoding ];

            return [ NSError errorWithDomain: TETextEditErrorDomain
                                        code: TETextEditSaveErrorEncodingInapplicable
                                    userInfo: @{ NSLocalizedDescriptionKey : [ NSString stringWithFormat: NSLocalizedString( @"This document can no longer saved using its original %@ encoding.", @"Title of alert panel informing user that the file's string encoding needs to be changed." )
                                                                                                                           , [ NSString localizedNameOfStringEncoding: encode ] ]
                                               , NSLocalizedRecoveryOptionsErrorKey : NSLocalizedString( @"Please choose another encoding (such as UTF-8).", @"Subtitle of alert panel informing that the file's string encoding needs to be changed" )
                                               , NSLocalizedFailureReasonErrorKey : NSLocalizedString( @"The specified text encoding isn\\U2019t applicable", @"Failure reason stating that the text encoding is not applicable." )
                                               , NSLocalizedRecoveryOptionsErrorKey :
                                                    @[ NSLocalizedString( @"OK", @"OK" )
                                                     , NSLocalizedString( @"Cancel", @"Button choice that allows the user to cancel." )
                                                     ]

                                               , NSRecoveryAttempterErrorKey : self } ];
            }
        }

    return nil;
    }

- ( BOOL ) checkAutosavingSafetyAfterChangeAndReturnError: ( NSError** ) _OutError
    {
    BOOL safe = YES;

	// it the document isn't saved, don't complain about limitations of its supposed backing store.
    if ( [ self fileURL ] )
        {
        if ( ![ [ self writableTypesForSaveOperation: NSSaveAsOperation ] containsObject: self.fileType ] )
            {
            if ( _OutError )
                *_OutError = [ self errorInTextEditDomainWithCode: TETextEditSaveErrorWritableTypeRequired ];

            safe = NO;
            }
        else if ( ![ self isRichText ] )
            {
            NSUInteger encoding = [ self _documentEncoding ];

            if ( encoding != TEEncodingManagerNoStringEncoding && ![ [ self._textStorage string ] canBeConvertedToEncoding: encoding ] )
                {
                if ( _OutError )
                    *_OutError = [ self errorInTextEditDomainWithCode: TETextEditSaveErrorWritableTypeRequired ];

                safe = NO;
                }
            }
        }

    return safe;
    }

- ( void ) updateChangeCount: ( NSDocumentChangeType ) _Change
    {
    NSError* error;

    // When a document is changed, it caeses to be transient.
    [ self set_isTransient: NO ];

    [ super updateChangeCount: _Change ];

    if ( _Change == NSChangeDone || _Change == NSChangeRedone )
        {
        // If we don't have a file URL, we can change our backing store type without consulting the suer.
        // NSDocument will update the extension of our autosaving location.
        // If we don't do this, we won't be able to store images in autosaved untitled documents.
        if ( ![ self fileURL ] )
            {
            if ( ![ [ self writableTypesForSaveOperation: NSSaveAsOperation ] containsObject: [ self fileType ] ] )
                [ self setFileType: (NSString*)( [ self._textStorage containsAttachments ] ? kUTTypeRTFD : kUTTypeRTF ) ];
            }
        else if ( ![ self checkAutosavingSafetyAfterChangeAndReturnError: &error ] )
            {
            void ( ^didRecoveryBlock )( BOOL ) = ^( BOOL _DidRecover )
                {
                if ( !_DidRecover )
                    {
                    if ( _Change == NSChangeDone || _Change == NSChangeRedone )
                        [ [ self undoManager ] undo ];
                    else if ( _Change == NSChangeUndone )
                        [ [ self undoManager ] redo ];
                    }
                };

            NSWindow* sheetWindow = [ self windowForSheet ];
            if ( sheetWindow )
                {
                [ self performActivityWithSynchronousWaiting: YES
                                                  usingBlock:
                    ^( void ( ^_ActivityCompletionHandler )() )
                        {
                        [ self presentError: error
                             modalForWindow: sheetWindow
                                   delegate: self
                         didPresentSelector: @selector( didPresentErrorWithRecovery:block: )
                                contextInfo:
                            Block_copy( ^( BOOL _DidRecover )
                                {
                                if ( !_DidRecover )
                                    {
                                    if ( _Change == NSChangeDone || _Change == NSChangeRedone )
                                        [ [ self undoManager ] undo ];
                                    else if ( _Change == NSChangeUndone )
                                        [ [ self undoManager ] redo ];
                                    }

                                _ActivityCompletionHandler();
                                } ) ];
                        } ];
                }
            else
                didRecoveryBlock( [ self presentError: error ] );
            }
        }
    }

- ( NSString* ) autosavingFileType
    {
    if ( [ self _isInDuplicate ] )
        if ( ![ [ self writableTypesForSaveOperation: NSSaveAsOperation ] containsObject: [ self fileType ] ] )
            return (NSString*)( [ self._textStorage containsAttachments ] ? kUTTypeRTFD : kUTTypeRTF );

    return [ super autosavingFileType ];
    }

/* When we duplicate a document, we need to temporarily return the autosaving file type for the
    resultant document.  Unfortunately, the only way to do this from a document subclass appears
    to be to use a boolean indicator.
 */
- ( NSDocument* ) duplicateAndReturnError: ( NSError** ) _OutError
    {
    NSDocument* result = nil;

    self._isInDuplicate = YES;
    result = [ super duplicateAndReturnError: _OutError ];
    self._isInDuplicate = NO;

    return result;
    }

- ( void ) document: ( NSDocument* ) _Ignored
            didSave: ( BOOL ) _DidSave
              block: ( void (^)( BOOL ) ) _Block
    {
    _Block( _DidSave );

    Block_release( _Block );
    }

- ( void ) didPresentErrorWithRecovery: ( BOOL ) _DidRecovery
                                 block: ( void (^)( BOOL ) ) _Block
    {
    _Block( _DidRecovery );

    Block_release( _Block );
    }

- ( void ) attemptRecoveryFromError: ( NSError* ) _Error
                        optionIndex: ( NSInteger ) _RecoveryOptionIndex
                           delegate: ( id ) _Delegate
                 didRecoverSelector: ( SEL ) _DidRecoverSelector
                        contextInfo: ( id ) _ContextInfo
    {
    BOOL didRecover = NO;
    if ( [ [ _Error domain ] isEqualToString: TETextEditErrorDomain ] )
        {
        NSInteger errorCode = [ _Error code ];

        switch ( errorCode )
            {
            case TETextEditSaveErrorWritableTypeRequired:
            case TETextEditSaveErrorConvertedDocument:
            case TETextEditSaveErrorLossyDocument:
                // duplicate button - index 0 for lossy document errors and index 2 otherwise
                if ( ( (errorCode == TETextEditSaveErrorLossyDocument) && (_RecoveryOptionIndex == 0) )
                  || ( (errorCode != TETextEditSaveErrorLossyDocument) && (_RecoveryOptionIndex == 2) ) )
                    {
                    NSError* duplicateError = nil;
                    NSDocument* duplicateDocument = [ self duplicateAndReturnError: &duplicateError ];

                    if ( !duplicateDocument )
                        {
                        NSWindow* sheetWindow = [ self windowForSheet ];

                        if ( sheetWindow )
                            {
                            [ _Delegate retain ];
                            [ self presentError: duplicateError
                                 modalForWindow: sheetWindow
                                       delegate: self
                             didPresentSelector: @selector( didPresentErrorWithRecovery:block: )
                                    contextInfo: Block_copy(
                                ^( BOOL _DidRecover )
                                    {
                                    [ _Delegate release ];
                                    ( (void (*)( id, SEL, BOOL, void* ))objc_msgSend )( _Delegate, _DidRecoverSelector, NO, _ContextInfo );
                                    } ) ];
                            return;
                            }
                        else
                            [ self presentError: duplicateError ];
                        }
                    }
                // save / overwrite button - index 2 for lossy document errors and 0 otherwise
                else if ( ( (errorCode == TETextEditSaveErrorLossyDocument) && (_RecoveryOptionIndex == 2) )
                       || ( (errorCode != TETextEditSaveErrorLossyDocument) && (_RecoveryOptionIndex == 0) ) )
                        {
                        if ( ![ [ self writableTypesForSaveOperation: NSSaveAsOperation ] containsObject: [ self fileType ] ] )
                            [ self setFileType: (NSString*)( [ self._textStorage containsAttachments ] ? kUTTypeRTFD : kUTTypeRTF ) ];

                        [ self set_isConvertedDocument: NO ];
                        [ self set_isLossyDocument: NO ];

                        didRecover = YES;
                        } break;
            case TETextEditSaveErrorEncodingInapplicable:
                if ( _RecoveryOptionIndex == 0 ) // OK Button
                    {
                    [ _Delegate retain ];

                    [ self continueActivityUsingBlock:
                        ^( void )
                            {
                            [ self runModalSavePanelForSaveOperation: NSSaveOperation
                                                            delegate: self
                                                     didSaveSelector: @selector( document:didSave:block: )
                                                         contextInfo: Block_copy(
                                ^( BOOL _DidSave )
                                    {
                                    [ _Delegate release ];
                                    ( (void (*)( id, SEL, BOOL, void* ))objc_msgSend )( _Delegate, _DidRecoverSelector, _DidSave, _ContextInfo );
                                    } ) ];
                            } ];
                        return;
                    } break;
            }
        }

    // call the delegate's didRecoverSelector
    ( (void (*)( id, SEL, BOOL, void* ))objc_msgSend)( _Delegate, _DidRecoverSelector, didRecover, _ContextInfo );
    }

// Returns an object that represents the document to be written to file.
- ( id ) fileWrapperOfType: ( NSString* ) _TypeName
                     error: ( NSError** ) _OutError
    {
    NSTextStorage* textStorage = [ self _textStorage ];
    NSRange range = NSMakeRange( 0, [ textStorage length ] );
    NSStringEncoding encode = 0;

    NSMutableDictionary* attrsDict = [ NSMutableDictionary dictionaryWithObjectsAndKeys:
          [ NSValue  valueWithSize: [ self paperSize ] ]                    , NSPaperSizeDocumentAttribute
        , [ NSNumber numberWithInteger: [ self _isReadOnly ] ? 1 : 0 ]      , NSReadOnlyDocumentAttribute
        , [ NSNumber numberWithFloat: [ self _hyphenationFactor ] ]         , NSHyphenationFactorDocumentAttribute
        , [ NSNumber numberWithDouble: [ [ self printInfo ] leftMargin ] ]  , NSLeftMarginDocumentAttribute
        , [ NSNumber numberWithDouble: [ [ self printInfo ] rightMargin ] ] , NSRightMarginDocumentAttribute
        , [ NSNumber numberWithDouble: [ [ self printInfo ] bottomMargin ] ], NSBottomMarginDocumentAttribute
        , [ NSNumber numberWithDouble: [ [ self printInfo ] topMargin ] ]   , NSTopMarginDocumentAttribute
        , [ NSNumber numberWithInteger: [ self _hasMultiplePages ] ? 1 : 0 ], NSViewModeDocumentAttribute
        , [ NSNumber numberWithBool: [ self _isUsesScreenFonts ] ]          , NSUsesScreenFontsDocumentAttribute
        , nil
        ];

    NSString* docType = nil;
    id val = nil; // temporary values

    NSSize size = [ self _viewSize ];
    if ( !NSEqualSize( size, NSZeroSize ) )
        attrsDict[ NSViewSizeDocumentAttribute ] = [ NSValue valueWithSize: size ];

    // TextEdit knows hot to save all thest types, including their super-types.
    // It does not know how to save any of their potential subtypes.
    // Hence, the conformance check is the reverse of the usual pattern.
    NSWorkspace* workspace = [ NSWorkspace sharedWorkspace ];

    // kUTTypePlainText also handles kUTTypeText and has to come before the other types so we will use the least specialized type
    // For example, kUTTypeText is an ancestor of kUTTypeText and kUTTypeRTF but we should use kUTTypeText because kUTTypeText is an ancestor of kUTTypeRTF.
    if ( [ workspace type: (NSString*)kUTTypePlainText conformsToType: _TypeName ] )        docType = NSPlainTextDocumentType;
    else if ( [ workspace type: (NSString*)kUTTypeRTF conformsToType: _TypeName ] )         docType = NSRTFTextDocumentType;
    else if ( [ workspace type: (NSString*)kUTTypeRTFD conformsToType: _TypeName ] )        docType = NSRTFDTextDocumentType;
    else if ( [ workspace type: kSimpleTextType conformsToType: _TypeName ] )               docType = NSMacSimpleTextDocumentType;
    else if ( [ workspace type: kWord97Type conformsToType: _TypeName ] )                   docType = NSDocFormatTextDocumentType;
    else if ( [ workspace type: kWord2007Type conformsToType: _TypeName ] )                 docType = NSOfficeOpenXMLTextDocumentType;
    else if ( [ workspace type: kWord2003XMLType conformsToType: _TypeName ] )              docType = NSWordMLTextDocumentType;
    else if ( [ workspace type: kOpenDocumentTextType conformsToType: _TypeName ] )         docType = NSOpenDocumentTextDocumentType;
    else if ( [ workspace type: (NSString*)kUTTypeHTML conformsToType: _TypeName ] )        docType = NSHTMLTextDocumentType;
    else if ( [ workspace type: (NSString*)kUTTypeWebArchive conformsToType: _TypeName ] )  docType = NSWebArchiveTextDocumentType;
    else [ NSException raise: NSInvalidArgumentException format: @"%@ is not a recognized document type.", _TypeName ];

    if ( docType )
        attrsDict[ NSDocumentTypeDocumentAttribute ] = docType;

    if ( [ self _hasMultiplePages ] &&  ([ self _scaleFactor ] != 1.f) )
        attrsDict[ NSViewZoomDocumentAttribute ] = [ NSNumber numberWithDouble: self._scaleFactor * 100.f ];

    if ( (val = [ self _backgroundColor ]) )
        attrsDict[ NSBackgroundColorDocumentAttribute ] = val;

    if ( docType == NSPlainTextDocumentType )
        {
        encode = [ self _documentEncoding ];

        if ( (self._currentSaveOperation == NSSaveOperation || self._currentSaveOperation == NSSaveAsOperation)
                && (self._documentEncodingForSaving != TEEncodingManagerNoStringEncoding) )
            encode = self._documentEncodingForSaving;

        if ( encode == TEEncodingManagerNoStringEncoding )
            encode = [ self suggestedDocumentEncoding ];

        attrsDict[ NSCharacterEncodingDocumentAttribute ] = [ NSNumber numberWithUnsignedInteger: encode ];
        }
    else if ( docType == NSHTMLTextDocumentType || docType == NSWebArchiveTextDocumentType )
        {
        NSUserDefaults* usrDefaults = [ NSUserDefaults standardUserDefaults ];
        NSMutableArray* excludedElements = [ NSMutableArray array ];

        if ( ![ usrDefaults boolForKey: TETextEditUseXHTMLDocType ] )
            [ excludedElements addObject: @"XML" ];

        if ( ![ usrDefaults boolForKey: TETextEditUseTransitionalDocType ] )
            [ excludedElements addObjectsFromArray: @[ @"APPLET", @"BASEFONT", @"CENTER"
                                                     , @"DIR",    @"FONT",     @"ISINDEX"
                                                     , @"MENU",   @"S",        @"STRIKE", @"U"
                                                     ] ];

        if ( ![ usrDefaults boolForKey: TETextEditEmbeddedCSS ] )
            {
            [ excludedElements addObject: @"STYLE" ];

            if ( ![ usrDefaults boolForKey: TETextEditUseInlineCSS ] )
                [ excludedElements addObject: @"SPAN" ];
            }

        if ( ![ usrDefaults boolForKey: TETextEditPreserveWhitespace ] )
            {
            [ excludedElements addObject: @"Apple-converted-space" ];
            [ excludedElements addObject: @"Apple-converted-tab"   ];
            [ excludedElements addObject: @"Apple-interchange-newline" ];
            }

        attrsDict[ NSExcludedElementsDocumentAttribute ] = excludedElements;
        attrsDict[ NSCharacterEncodingDocumentAttribute ] = [ usrDefaults objectForKey: TETextEditHTMLEncoding ];
        attrsDict[ NSPrefixSpacesDocumentAttribute ] = @2;
        }

        // Set the text layout orientation for each page
        if ( (val = [ [ [ self windowControllers ] objectAtIndex: 0 ] layoutOrientationSections ]) )
            attrsDict[ NSTextLayoutSectionsAttribute ] = val;

        // Set the document properties, generically, going through key value coding
        for ( NSString* property in [ self knownDocumentProperties ] )
            {
            id value = [ self valueForKey: property ];

            if ( value && ![ value isEqual: @"" ]
                       && ![ value isEqual: @[] ] )
                attrsDict[ [ [ self documentPropertyToAttributeNameMappings ] objectForKey: property ] ] = value;
            }

        NSFileWrapper* result = nil;
        if ( docType == NSRTFDTextDocumentType || (docType == NSPlainTextDocumentType && !self._isOpenedIgnoringRighText) )
            {
            // We obtain a file wrapper from the text storage for RTFD (to produce a directory), or for true plain-text documents (to write out encoding in extended attributes)
            result = [ textStorage fileWrapperFromRange: range
                                     documentAttributes: attrsDict
                                                  error: _OutError ]; // returns NSFileWrapper
            }
        else
            {
            NSData* data = [ textStorage dataFromRange: range
                                    documentAttributes: attrsDict
                                                 error: _OutError ];  // returns NSData
            if ( data )
                {
                result = [ [ [ NSFileWrapper alloc ] initRegularFileWithContents: data ] autorelease ];

                if ( !result && _OutError )
                    *_OutError = [ NSError errorWithDomain: NSCocoaErrorDomain
                                                      code: NSFileWriteUnknownError
                                                  userInfo: nil ];  // Unlikely, but just in case we should generate an NSError
                }
            }

        if ( result && docType == NSPlainTextDocumentType
                    && (self._currentSaveOperation == NSSaveOperation || self._currentSaveOperation == NSSaveAsOperation) )
            [ self set_documentEncoding: encode ];

    return result;
    }

- ( BOOL ) checkAutosavinGSafetyAndReturnError: ( NSError** ) _OutError
    {
    BOOL safe = YES;

    if ( ![ super checkAutosavingSafetyAndReturnError: _OutError ] )
        return NO;

    if ( [ self fileURL ] )
        {
        // If the document is converted or lossy but can't be saved in its file type,
        // we will need to save it in a different location or duplicate it anyway.
        // Therefore, we should tell the user that a writable type is required instead.
        if ( ![ [ self writableTypesForSaveOperation: NSSaveAsOperation ignoreTemporaryState: YES ]  containsObject: [ self fileType ] ] )
            {
            if ( _OutError )
                *_OutError = [ self errorInTextEditDomainWithCode: TETextEditSaveErrorWritableTypeRequired ];

            safe = NO;
            }
        else if ( [ self _isConvertedDocument ] )
            {
            if ( _OutError )
                *_OutError = [ self errorInTextEditDomainWithCode: TETextEditSaveErrorConvertedDocument ];

            safe = NO;
            }
        else if ( [ self _isLossyDocument ] )
            {
            if ( _OutError )
                *_OutError = [ self errorInTextEditDomainWithCode: TETextEditSaveErrorLossyDocument ];

            safe = NO;
            }
        }

    return safe;
    }

/* For plain-text documents, we add our own accessory view for selecting encodings.
 * The plain text case does not require a format popup. */
- ( BOOL ) shouldRunSavePanelWithAccessoryView
    {
    return [ self isRichText ];
    }

/* If the document is a converted version of a document that existed on disk, 
 * set the default directory to the directory in which the source file (converted file) resided at the time the document was converted. 
 * If the document is plain text, we additionally add an encoding popup. */
- ( BOOL ) prepareSavePanel: ( NSSavePanel* ) _SavePanel
    {
    NSPopUpButton* encodingPopup = nil;
    NSButton* extensionCheckbox = nil;
    NSString* string;

    if ( ![ self isRichText ] )
        {
        BOOL isAddExtension = [ USER_DEFAULTS boolForKey: TETextEditAddExtensionToNewPlainTextFiles ];

        // If no encoding, figure out which encoding should be default in encoding popup, set as document encoding.
        string = [ self._textStorage string ];
        NSStringEncoding encode = [ self _documentEncoding ];

        [ self set_documentEncodingForSaving:
            (encode == TEEncodingManagerNoStringEncoding || ![ string canBeConvertedToEncoding: encode ]) ? [ self suggestedDocumentEncoding ] : encode ];

        NSView* accessoryView = [ [ [ NSDocumentController sharedDocumentController ] class ] encodingAccessory:[self _documentEncodingForSaving] includeDefaultEntry:NO encodingPopUp:&encodingPopup checkBox:&extensionCheckbox];
        accessoryView.translatesAutoresizingMaskIntoConstraints = NO;

        [ _SavePanel setAccessoryView: accessoryView ];

        // Set up the checkbox
        [ extensionCheckbox setTitle: NSLocalizedString( @"If no extension is provided, use \".txt\""
                                                       , @"Checkbox indicating that if the user does not specify an extension when saving a plain text file, .txt will used" ) ];
        [ extensionCheckbox setToolTip: NSLocalizedString( @"Automatically append \".txt\" to the file name if no known file name extension is provided."
                                                       , @"Tooltip for checkbox indicating that if the user does not specify an extension when saving a plain text file, .txt will be used" ) ];
        [ extensionCheckbox setState: isAddExtension ];
        [ extensionCheckbox setAction: @selector( appendPlainTextExtensionChanged: ) ];
        [ extensionCheckbox setTarget: self ];

        if ( isAddExtension )
            {
            [ _SavePanel setAllowsOtherFileTypes: YES ];
            [ _SavePanel setAllowedFileTypes: @[ (NSString*)kUTTypePlainText ] ];
            }
        else
            {
            // NSDocument defaults to setting the allowedFileType to kUTTypePlainText,
            // which gives the fileName a ".txt" extension.
            // We want don't want to append the extension for Untitled documents.
            // First we clear out the allowedFileType that NSDocument set.
            // We want to allow anything, so we pass 'nil'.
            // This will prevent NSSavePanel from appending an extension.
            [ _SavePanel setAllowedFileTypes: nil ];

            // If this document was previously saved, use the URL's name
            NSString* fileName;
            BOOL gotFileName = [ [ self fileURL ] getResourceValue: &fileName
                                                            forKey: NSURLNameKey
                                                             error: nil ];
            // If the document has not yet been saved, or we couldn't find the filename, then use the displayName.
            if ( !gotFileName || !fileName )
                fileName = [ self displayName ];

            [ _SavePanel setNameFieldStringValue: fileName ];
            }

        // Further set up the encoding popup
        NSUInteger cnt = [ encodingPopup numberOfItems ];
        if ( cnt * [ string length ] < 5000000 )
            {
            while ( cnt-- )
                {
                NSStringEncoding encoding = (NSStringEncoding)[ [ [ encodingPopup itemAtIndex: cnt ] representedObject ] unsignedIntegerValue ];

                // Hardwire some encodings known to allow any content
                if ( (encoding != TEEncodingManagerNoStringEncoding)
                        && (encoding != NSUnicodeStringEncoding)
                        && (encoding != NSUTF8StringEncoding)
                        && (encoding != NSNonLossyASCIIStringEncoding)
                        && ![ string canBeConvertedToEncoding: encoding ] )
                    [ [ encodingPopup itemAtIndex: cnt ] setEnabled: NO ];
                }
            }

        [ encodingPopup setAction: @selector( encodingPopupChanged: ) ];
        [ encodingPopup setTarget: self ];
        }

    return YES;
    }

@end // TEDocument + TETextEditDocumentOverrides

/* Truncate string to no longer than truncationLength; should be > 10 */
NSString* truncatedString( NSString* _Str, NSUInteger _TruncationLength )
    {
    NSUInteger length = [ _Str length ];

    if ( length < _TruncationLength )
        return _Str;

    return [ [ _Str substringToIndex: _TruncationLength - 10 ] stringByAppendingString: @"\u2026" ];
    }

////////////////////////////////////////////////////////////////////////////

/****************************************************************************
 **                                                                        **
 **      _________                                      _______            **
 **     |___   ___|                                   / ______ \           **
 **         | |     _______   _______   _______      | /      |_|          **
 **         | |    ||     || ||     || ||     ||     | |    _ __           **
 **         | |    ||     || ||     || ||     ||     | |   |__  \          **
 **         | |    ||     || ||     || ||     ||     | \_ _ __| |  _       **
 **         |_|    ||_____|| ||     || ||_____||      \________/  |_|      **
 **                                           ||                           **
 **                                    ||_____||                           **
 **                                                                        **
 ***************************************************************************/
///:~