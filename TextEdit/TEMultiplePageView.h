///:
/*****************************************************************************
 **                                                                         **
 **                               .======.                                  **
 **                               | INRI |                                  **
 **                               |      |                                  **
 **                               |      |                                  **
 **                      .========'      '========.                         **
 **                      |   _      xxxx      _   |                         **
 **                      |  /_;-.__ / _\  _.-;_\  |                         **
 **                      |     `-._`'`_/'`.-'     |                         **
 **                      '========.`\   /`========'                         **
 **                               | |  / |                                  **
 **                               |/-.(  |                                  **
 **                               |\_._\ |                                  **
 **                               | \ \`;|                                  **
 **                               |  > |/|                                  **
 **                               | / // |                                  **
 **                               | |//  |                                  **
 **                               | \(\  |                                  **
 **                               |  ``  |                                  **
 **                               |      |                                  **
 **                               |      |                                  **
 **                               |      |                                  **
 **                               |      |                                  **
 **                   \\    _  _\\| \//  |//_   _ \// _                     **
 **                  ^ `^`^ ^`` `^ ^` ``^^`  `^^` `^ `^                     **
 **                                                                         **
 **                       Copyright (c) 2014 Tong G.                        **
 **                          ALL RIGHTS RESERVED.                           **
 **                                                                         **
 ****************************************************************************/

#import <Cocoa/Cocoa.h>

// TEMultiplePageView
@interface TEMultiplePageView : NSView

@property ( retain ) NSPrintInfo* _printInfo;
@property ( retain ) NSColor* _lineColor;
@property ( retain ) NSColor* _marginColor;

@property ( assign ) NSUInteger _numPages;
@property ( assign ) NSTextLayoutOrientation _layoutOrientation;

- ( void ) set_printInfo: ( NSPrintInfo* )_PrintInfo;
- ( NSPrintInfo* ) _printInfo;

- ( CGFloat ) pageSeparatorHeight;
- ( NSSize ) documentSizeInPage;    /* Returns the area where the document can draw */
- ( NSRect ) documentRectForPageNumber: ( NSUInteger )_PageNumber;  /* First page is page 0 */
- ( NSRect ) pageRectForPageNumber: ( NSUInteger )_PageNumber;      /* First page is page 0 */

- ( void ) set_numberOfPages: ( NSUInteger )_Num;
- ( NSUInteger ) _numberOfPages;

- ( void ) set_layoutOrientation: ( NSTextLayoutOrientation )_Orientation;
- ( NSTextLayoutOrientation ) _layoutOrientation;

@end // TEMultiplePageView

////////////////////////////////////////////////////////////////////////////

/****************************************************************************
 **                                                                        **
 **      _________                                      _______            **
 **     |___   ___|                                   / ______ \           **
 **         | |     _______   _______   _______      | /      |_|          **
 **         | |    ||     || ||     || ||     ||     | |    _ __           **
 **         | |    ||     || ||     || ||     ||     | |   |__  \          **
 **         | |    ||     || ||     || ||     ||     | \_ _ __| |  _       **
 **         |_|    ||_____|| ||     || ||_____||      \________/  |_|      **
 **                                           ||                           **
 **                                    ||_____||                           **
 **                                                                        **
 ***************************************************************************/
///:~