///:
/*****************************************************************************
 **                                                                         **
 **                               .======.                                  **
 **                               | INRI |                                  **
 **                               |      |                                  **
 **                               |      |                                  **
 **                      .========'      '========.                         **
 **                      |   _      xxxx      _   |                         **
 **                      |  /_;-.__ / _\  _.-;_\  |                         **
 **                      |     `-._`'`_/'`.-'     |                         **
 **                      '========.`\   /`========'                         **
 **                               | |  / |                                  **
 **                               |/-.(  |                                  **
 **                               |\_._\ |                                  **
 **                               | \ \`;|                                  **
 **                               |  > |/|                                  **
 **                               | / // |                                  **
 **                               | |//  |                                  **
 **                               | \(\  |                                  **
 **                               |  ``  |                                  **
 **                               |      |                                  **
 **                               |      |                                  **
 **                               |      |                                  **
 **                               |      |                                  **
 **                   \\    _  _\\| \//  |//_   _ \// _                     **
 **                  ^ `^`^ ^`` `^ ^` ``^^`  `^^` `^ `^                     **
 **                                                                         **
 **                       Copyright (c) 2014 Tong G.                        **
 **                          ALL RIGHTS RESERVED.                           **
 **                                                                         **
 ****************************************************************************/

#import "TEMultiplePageView.h"
#import "TETextEditMisc.h"

// TEMultiplePageView
@implementation TEMultiplePageView

@synthesize _printInfo;
@synthesize _lineColor;
@synthesize _marginColor;

@synthesize _numPages;
@synthesize _layoutOrientation;

#pragma mark -
#pragma mark Overrides for view setting
- ( id ) initWithFrame: ( NSRect )_Rect
    {
    if ( ( self = [ super initWithFrame: _Rect ] ) )
        {
        self._numPages = 0;

        [ self set_lineColor: [ NSColor lightGrayColor ] ];
        [ self set_marginColor: [ NSColor whiteColor ] ];

        /* This will set the frame to whatever's appropriate... */
        [ self set_printInfo: [ NSPrintInfo sharedPrintInfo ] ];
        }

    return self;
    }

- ( void ) dealloc
    {
    [ self._printInfo release ];
    [ super dealloc ];
    }

- ( BOOL ) isFlipped
    {
    return YES;
    }

- ( BOOL ) isOpaque
    {
    return YES;
    }

#pragma mark -
#pragma mark Overrides for printing
/**** Smart magnification ****/
- (NSRect)rectForSmartMagnificationAtPoint: ( NSPoint )_Location
                                    inRect: ( NSRect )_VisibleRect
{
    NSRect result;
    NSUInteger pageNumber = [self pageNumberForPoint:_Location];
    NSRect documentRect = NSInsetRect([self documentRectForPageNumber:pageNumber], -3.0, -3.0);  // We use -3 to show a bit of the margins
    NSRect pageRect = [self pageRectForPageNumber:pageNumber];
    
    if (NSPointInRect(_Location, documentRect)) {        // Smart magnify on page contents; return the page contents rect
        result = documentRect;
    } else if (NSPointInRect(_Location, pageRect)) {     // Smart magnify on page margins; return the page rect (not including separator area)
        result = pageRect;
    } else {        // Smart magnify between pages, or the empty area beyond the side or bottom/top of the page; return the extended area for the page
        result = pageRect;
        if (NSTextLayoutOrientationHorizontal == self._layoutOrientation)
        {
            if (NSMaxX(_VisibleRect) > NSMaxX(pageRect)) result.size.width = NSMaxX(_VisibleRect);        // include area to the right of the paper
            if (pageNumber + 1 < self._numPages)              result.size.height += [self pageSeparatorHeight];
            if (_Location.y > NSMaxY(result))            result.size.height = ceil(_Location.y - result.origin.y);   // extend the rect out to include location
        } else {
            if (NSMaxY(_VisibleRect) > NSMaxY(pageRect)) result.size.height = NSMaxY(_VisibleRect);       // include area below the paper
            if (pageNumber + 1 < self._numPages)              result.size.width += [self pageSeparatorHeight];
            if (_Location.x > NSMaxX(result))            result.size.width = ceil(_Location.x - result.origin.x);    // extend the rect out to include location
        }
    }
    return result;
}

/**** Printing support... ****/
- ( BOOL ) knowsPageRange: ( NSRangePointer )_RangePtr
    {
    _RangePtr->length = [ self _numberOfPages ];

    return YES;
    }

- ( NSRect ) rectForPage: ( NSInteger )_Page
    {
    /* Our page numbers start from 0; the kit's from 1 */
    return [ self documentRectForPageNumber: _Page - 1 ];
    }

#pragma mark -
#pragma mark Misc
- ( void ) updateFrame
    {
    if ( [ self superview ] )
        {
        NSRect rect = NSZeroRect;
        rect.size = [ self._printInfo paperSize ];

        if ( self._layoutOrientation == NSTextLayoutOrientationHorizontal )
            {
            rect.size.height = rect.size.height * self._numPages;

            if ( self._numPages > 1 )
                rect.size.height += [ self pageSeparatorHeight ] * ( self._numPages - 1 );
            }
        else if ( self._layoutOrientation == NSTextLayoutOrientationVertical )
            {
            rect.size.width = rect.size.width * self._numPages;

            if ( self._numPages > 1 )
                rect.size.width += [ self pageSeparatorHeight ] * ( self._numPages - 1 );
            }

        rect.size = [ self convertSize: rect.size toView: [ self superview ] ];
        [ self setFrame: rect ];
        }
    }

- ( void ) set_printInfo: ( NSPrintInfo* )_PrintInfo
    {
    if ( _printInfo != _PrintInfo )
        {
        [ _printInfo autorelease ];
        _printInfo = [ _PrintInfo copy ];

        [ self updateFrame ];
        [ self setNeedsDisplay: YES ];  /* Because the page size or margins might change (could optimize this) */
        }
    }

- ( NSPrintInfo* ) _printInfo
    {
    return _printInfo;
    }

- ( CGFloat ) pageSeparatorHeight
    {
    return .5f;
    }

- ( NSSize ) documentSizeInPage
    {
    return TEDocumentSizeForPrintInfo( self._printInfo );
    }

- ( NSRect ) documentRectForPageNumber: ( NSUInteger )_PageNumber
    {
    /* First page is page 0, of course! */
    NSRect rect = [ self pageRectForPageNumber: _PageNumber ];

    rect.origin.x += [ self._printInfo leftMargin ] - TEDefaultTextPadding();
    rect.origin.y += [ self._printInfo topMargin ];
    rect.size = [ self documentSizeInPage ];

    return rect;
    }

- ( NSRect ) pageRectForPageNumber: ( NSUInteger )_PageNumber
    {
    NSRect rect;

    rect.size = [ self._printInfo paperSize ];
    rect.origin = [ self frame ].origin;

    if ( NSTextLayoutOrientationHorizontal == self._layoutOrientation )
        rect.origin.y += ( (rect.size.height + [ self pageSeparatorHeight ]) * _PageNumber );
    else
        rect.origin.x += ( NSWidth([ self bounds ])
                            - ( (rect.size.width + [ self pageSeparatorHeight ]) * (_PageNumber + 1) ) );

    return rect;
    }

/* For locations on the page separator right after a page, returns that page number.  
 * Same for any locations on the empty (gray background) area to the side of a page. 
 * Will return 0 or numPages-1 for locations beyond the ends. Results are 0-based.
 */
- (NSUInteger)pageNumberForPoint:(NSPoint)loc {
    NSUInteger pageNumber;
    if (NSTextLayoutOrientationHorizontal == self._layoutOrientation) {
        if (loc.y < 0) pageNumber = 0;
        else if (loc.y >= [self bounds].size.height) pageNumber = self._numPages - 1;
        else pageNumber = loc.y / ([self._printInfo paperSize].height + [self pageSeparatorHeight]);
    } else {
        if (loc.x < 0) pageNumber = self._numPages - 1;
        else if (loc.x >= [self bounds].size.width) pageNumber = 0;
        else pageNumber = (NSWidth([self bounds]) - loc.x) / ([self._printInfo paperSize].width + [self pageSeparatorHeight]);
    }
    return pageNumber;    
}

- ( void ) set_numberOfPages: ( NSUInteger )_Num
    {
    if ( self._numPages != _Num )
        {
        NSRect oldFrame = [ self frame ];
        NSRect newFrame;
        _numPages = _Num;

        [ self updateFrame ];
        newFrame = [ self frame ];

        if ( newFrame.size.height > oldFrame.size.height )
            {
            NSRect rect = NSMakeRect( oldFrame.origin.x,   NSMaxY( oldFrame )
                                    , oldFrame.size.width, NSMaxY( newFrame ) - NSMaxY( oldFrame ) );

            [ self setNeedsDisplayInRect: rect ];
            }
        }
    }

- ( NSUInteger ) _numberOfPages
    {
    return _numPages;
    }

- ( void ) set_layoutOrientation: ( NSTextLayoutOrientation )_Orientation
    {
    if ( _Orientation != _layoutOrientation )
        {
        _layoutOrientation = _Orientation;

        [ self updateFrame ];
        }
    }

- ( NSTextLayoutOrientation ) _layoutOrientation
    {
    return _layoutOrientation;
    }

- ( void ) drawRect: ( NSRect )_Rect
    {
    if ( [ [ NSGraphicsContext currentContext ] isDrawingToScreen ] )
        {
        NSSize paperSize = [ self._printInfo paperSize ];

        NSUInteger firstPage = 0;
        NSUInteger lastPage = 0;

        if ( self._layoutOrientation == NSTextLayoutOrientationHorizontal )
            {
            firstPage = NSMinY( _Rect ) / (paperSize.height + [ self pageSeparatorHeight ]);
            lastPage = NSMinY( _Rect ) / (paperSize.height + [ self pageSeparatorHeight ]);
            }
        else if ( self._layoutOrientation == NSTextLayoutOrientationVertical )
            {
            firstPage = self._numPages - ( NSMaxX( _Rect ) / (paperSize.width + [ self pageSeparatorHeight ]) );
            lastPage = self._numPages - ( NSMinX( _Rect ) / (paperSize.width + [ self pageSeparatorHeight ]) );
            }

        [ self._marginColor set ];
        NSRectFill( _Rect );

        [ self._lineColor set ];
        for ( NSUInteger cnt = firstPage; cnt <= lastPage; cnt++ )
            {
            // Draw boundary around the page, making sure it doesn't overlap the document area in terms of pixels.
            NSRect centerScanRect = [ self documentRectForPageNumber: cnt ];
            NSRect docRect = NSInsetRect( [ self centerScanRect: centerScanRect ], -1.f, -1.f );

            NSFrameRectWithWidth( docRect, 1.f );
            }

        if ( [[self superview] isKindOfClass: [NSClipView class]] )
            {
            NSColor* backgroundColor = [ ( NSClipView* )[ self superview ] backgroundColor ];
            [ backgroundColor set ];

            for ( NSUInteger cnt = firstPage; cnt <= lastPage; cnt++ )
                {
                NSRect pageRect = [ self pageRectForPageNumber: cnt ];
                NSRect separatorRect;

                if ( self._layoutOrientation == NSTextLayoutOrientationHorizontal )
                    separatorRect = NSMakeRect( NSMinX( pageRect ), NSMaxY( pageRect ), NSWidth( pageRect ), [ self pageSeparatorHeight ] );
                else
                    separatorRect = NSMakeRect( NSMaxX( pageRect ), NSMinY( pageRect ), [ self pageSeparatorHeight ], NSHeight( pageRect ) );

                NSRectFill( separatorRect );
                }
            }
        }
    }

@end // TEMultiplePageView

////////////////////////////////////////////////////////////////////////////

/****************************************************************************
 **                                                                        **
 **      _________                                      _______            **
 **     |___   ___|                                   / ______ \           **
 **         | |     _______   _______   _______      | /      |_|          **
 **         | |    ||     || ||     || ||     ||     | |    _ __           **
 **         | |    ||     || ||     || ||     ||     | |   |__  \          **
 **         | |    ||     || ||     || ||     ||     | \_ _ __| |  _       **
 **         |_|    ||_____|| ||     || ||_____||      \________/  |_|      **
 **                                           ||                           **
 **                                    ||_____||                           **
 **                                                                        **
 ***************************************************************************/
///:~