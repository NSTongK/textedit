///:
/*****************************************************************************
 **                                                                         **
 **                               .======.                                  **
 **                               | INRI |                                  **
 **                               |      |                                  **
 **                               |      |                                  **
 **                      .========'      '========.                         **
 **                      |   _      xxxx      _   |                         **
 **                      |  /_;-.__ / _\  _.-;_\  |                         **
 **                      |     `-._`'`_/'`.-'     |                         **
 **                      '========.`\   /`========'                         **
 **                               | |  / |                                  **
 **                               |/-.(  |                                  **
 **                               |\_._\ |                                  **
 **                               | \ \`;|                                  **
 **                               |  > |/|                                  **
 **                               | / // |                                  **
 **                               | |//  |                                  **
 **                               | \(\  |                                  **
 **                               |  ``  |                                  **
 **                               |      |                                  **
 **                               |      |                                  **
 **                               |      |                                  **
 **                               |      |                                  **
 **                   \\    _  _\\| \//  |//_   _ \// _                     **
 **                  ^ `^`^ ^`` `^ ^` ``^^`  `^^` `^ `^                     **
 **                                                                         **
 **                       Copyright (c) 2014 Tong G.                        **
 **                          ALL RIGHTS RESERVED.                           **
 **                                                                         **
 ****************************************************************************/

#import "TETextEditDefaultsKeys.h"

// Note that not all keys in this file are exposed in the UI.
// Some of them are not used at all, but are kept for reference.
NSString* const TETextEditRichText                              = @"RichText";
NSString* const TETextEditShowPageBreaks                        = @"ShowPageBreaks";
NSString* const TETextEditAddExtensionToNewPlainTextFiles       = @"AddExtensionToNewPlainTextFiles";
NSString* const TETextEditWindowWidth                           = @"WindowWidth";
NSString* const TETextEditWindowHeight                          = @"WindowHeight";
NSString* const TETextEditPlainTextEncodingForRead              = @"TextEncodingForRead";
NSString* const TETextEditPlainTextEncodingForWrite             = @"TextEncodingForWrite";
NSString* const TETextEditIgnoreRichText                        = @"IgnoreRichText";
NSString* const TETextEditIgnoreHTML                            = @"IgnoreHTML";
NSString* const TETextEditTabWidth                              = @"TabWidth";
NSString* const TETextEditForegroundLayoutToIndex               = @"ForegroundLayoutToIndex";
NSString* const TETextEditOpenPanelFollowsMainWindow            = @"OpenPanelFollowsMainWindow";
NSString* const TETextEditCheckSpellingAsYouType                = @"CheckSpellingAsYouType";
NSString* const TETextEditCheckGrammarWithSpelling              = @"CheckGrammarWithSpelling";
NSString* const TETextEditCorrectSpellingAutomatically          = @"CorrectSpellingAutomatically";
NSString* const TETextEditShowRuler                             = @"ShowRuler";
NSString* const TETextEditSmartCopyPaste                        = @"SmartCopyPaste";
NSString* const TETextEditSmartQuotes                           = @"SmartQuotes";
NSString* const TETextEditSmartDashes                           = @"SmartDashes";
NSString* const TETextEditSmartLinks                            = @"SmartLinks";
NSString* const TETextEditDataDetectors                         = @"DataDetectors";
NSString* const TETextEditTextReplacement                       = @"DataDetectors";
NSString* const TETextEditSubstitutionsEnabledInRichTextOnly    = @"SubstitutionsEnabledInRichTextOnly";
NSString* const TETextEditUseXHTMLDocType                       = @"UseXHTMLDocType";
NSString* const TETextEditUseTransitionalDocType                = @"UseTransitionalDocType";
NSString* const TETextEditEmbeddedCSS                           = @"EmbeddedCSS";
NSString* const TETextEditUseInlineCSS                          = @"UseInlineCSS";
NSString* const TETextEditHTMLEncoding                          = @"HTMLEncoding";
NSString* const TETextEditPreserveWhitespace                    = @"PreserveWhitespace";
NSString* const TETextEditAutosavingDelay                       = @"AutosavingDelay";
NSString* const TETextEditNumberPagesWhenPrinting               = @"NumberPagesWhenPrinting";
NSString* const TETextEditWrapToFitWhenPrinting                 = @"WrapToFitWhenPrinting";
NSString* const TETextEditUseScreenFonts                        = @"UseScreenFonts";

// Use different convention for the key values here, to be consistent with the keys in Document
NSString* const TETextEditAuthorProperty                        = @"author";
NSString* const TETextEditCompanyProperty                       = @"company";
NSString* const TETextEditCopyrightProperty                     = @"copyright";

NSString* const TETextEidtAppleMeasurementUnits                 = @"AppleMeasurementUnits";

////////////////////////////////////////////////////////////////////////////

/****************************************************************************
 **                                                                        **
 **      _________                                      _______            **
 **     |___   ___|                                   / ______ \           **
 **         | |     _______   _______   _______      | /      |_|          **
 **         | |    ||     || ||     || ||     ||     | |    _ __           **
 **         | |    ||     || ||     || ||     ||     | |   |__  \          **
 **         | |    ||     || ||     || ||     ||     | \_ _ __| |  _       **
 **         |_|    ||_____|| ||     || ||_____||      \________/  |_|      **
 **                                           ||                           **
 **                                    ||_____||                           **
 **                                                                        **
 ***************************************************************************/
///:~