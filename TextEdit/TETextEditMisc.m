///:
/*****************************************************************************
 **                                                                         **
 **                               .======.                                  **
 **                               | INRI |                                  **
 **                               |      |                                  **
 **                               |      |                                  **
 **                      .========'      '========.                         **
 **                      |   _      xxxx      _   |                         **
 **                      |  /_;-.__ / _\  _.-;_\  |                         **
 **                      |     `-._`'`_/'`.-'     |                         **
 **                      '========.`\   /`========'                         **
 **                               | |  / |                                  **
 **                               |/-.(  |                                  **
 **                               |\_._\ |                                  **
 **                               | \ \`;|                                  **
 **                               |  > |/|                                  **
 **                               | / // |                                  **
 **                               | |//  |                                  **
 **                               | \(\  |                                  **
 **                               |  ``  |                                  **
 **                               |      |                                  **
 **                               |      |                                  **
 **                               |      |                                  **
 **                               |      |                                  **
 **                   \\    _  _\\| \//  |//_   _ \// _                     **
 **                  ^ `^`^ ^`` `^ ^` ``^^`  `^^` `^ `^                     **
 **                                                                         **
 **                       Copyright (c) 2014 Tong G.                        **
 **                          ALL RIGHTS RESERVED.                           **
 **                                                                         **
 ****************************************************************************/

#import "TETextEditMisc.h"

/* Returns the default padding on the left/right edges of textViews. */
CGFloat TEDefaultTextPadding( void )
    {
    CGFloat static padding = -1;

    if ( padding < .0f )
        {
        NSTextContainer* textContainer = [ [ [ NSTextContainer alloc ] init ] autorelease ];
        padding = [ textContainer lineFragmentPadding ];
        }

    return padding;
    }

/* Helper used in toggling menu items in validate methods, based on a condition (useFirst) */
NSInteger static kTagForFirst = 42;
NSInteger static kTagForSecond = 43;

void TEValidateToggleItem( NSMenuItem* _MenuItem, BOOL _UseFirst
                       , NSString* _First, NSString* _Second )
    {
    if ( _UseFirst )
        {
        if ( [ _MenuItem tag ] != kTagForFirst )
            {
            [ _MenuItem setTitleWithMnemonic: _First ];
            [ _MenuItem setTag: kTagForFirst ];
            }
        }
    else
        {
        if ( [ _MenuItem tag ] != kTagForSecond )
            {
            [ _MenuItem setTitleWithMnemonic: _Second ];
            [ _MenuItem setTag: kTagForSecond ];
            }
        }
    }

/* Truncate string to no longer than _Truncation; should be > 10 */
NSString* TETruncatedString( NSString* _String, NSUInteger _TruncationLength )
    {
    NSUInteger length = [ _String length ];

    if ( length < _TruncationLength )
        return _String;

    return [ [ _String substringToIndex: _TruncationLength - 10 ] stringByAppendingString: @"..." ];
    }

/* Return the text view size appropriate for the NSPrintInfo */
NSSize TEDocumentSizeForPrintInfo( NSPrintInfo* _PrintInfo )
    {
    NSSize paperSize = [ _PrintInfo paperSize ];

    paperSize.width -= ( [_PrintInfo leftMargin] + [_PrintInfo rightMargin] ) - TEDefaultTextPadding() * 2.f;
    paperSize.height -= ( [_PrintInfo topMargin] + [_PrintInfo bottomMargin] );

    return paperSize;
    }

/* A method on NSTextView to cause layout up to the specified index */
@implementation NSTextView ( TETextEditAdditions )

/* This method causes the text to be laid out in foreground (approximately) up to the indicated charater index.
 * Note that since we are adding a category on a system framework,
 * we are prefixing the method with "textEdit" to greatly reduce change of any naming conflict.
 */
- ( void ) textEditDoForegroundLayoutToCharacterIndex: ( NSUInteger )_Location
    {
    NSUInteger length = 0UL;

    if ( _Location > 0 && (length = [[self textStorage] length ]) > 0 )
        {
        NSRange glyphRange;

        if ( _Location >= length )
            _Location = length - 1;

        /* Find out which glyph index the desired character index correspond to */
        glyphRange = [ [ self layoutManager ] glyphRangeForCharacterRange: NSMakeRange( _Location, 1 )
                                                     actualCharacterRange: nil ];
        if ( glyphRange.location > 0 )
            /* Now cause layout by asking a question which has to determine where the glyph is */
            ( void )[ [ self layoutManager ] textContainerForGlyphAtIndex: glyphRange.location - 1
                                                           effectiveRange: nil ];
        }
    }

@end

////////////////////////////////////////////////////////////////////////////

/****************************************************************************
 **                                                                        **
 **      _________                                      _______            **
 **     |___   ___|                                   / ______ \           **
 **         | |     _______   _______   _______      | /      |_|          **
 **         | |    ||     || ||     || ||     ||     | |    _ __           **
 **         | |    ||     || ||     || ||     ||     | |   |__  \          **
 **         | |    ||     || ||     || ||     ||     | \_ _ __| |  _       **
 **         |_|    ||_____|| ||     || ||_____||      \________/  |_|      **
 **                                           ||                           **
 **                                    ||_____||                           **
 **                                                                        **
 ***************************************************************************/
///:~