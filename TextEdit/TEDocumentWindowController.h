///:
/*****************************************************************************
 **                                                                         **
 **                               .======.                                  **
 **                               | INRI |                                  **
 **                               |      |                                  **
 **                               |      |                                  **
 **                      .========'      '========.                         **
 **                      |   _      xxxx      _   |                         **
 **                      |  /_;-.__ / _\  _.-;_\  |                         **
 **                      |     `-._`'`_/'`.-'     |                         **
 **                      '========.`\   /`========'                         **
 **                               | |  / |                                  **
 **                               |/-.(  |                                  **
 **                               |\_._\ |                                  **
 **                               | \ \`;|                                  **
 **                               |  > |/|                                  **
 **                               | / // |                                  **
 **                               | |//  |                                  **
 **                               | \(\  |                                  **
 **                               |  ``  |                                  **
 **                               |      |                                  **
 **                               |      |                                  **
 **                               |      |                                  **
 **                               |      |                                  **
 **                   \\    _  _\\| \//  |//_   _ \// _                     **
 **                  ^ `^`^ ^`` `^ ^` ``^^`  `^^` `^ `^                     **
 **                                                                         **
 **                       Copyright (c) 2014 Tong G.                        **
 **                          ALL RIGHTS RESERVED.                           **
 **                                                                         **
 ****************************************************************************/

#import <Cocoa/Cocoa.h>

#import "TEScallingScrollView.h"

// TEDocumentWindowController class
@interface TEDocumentWindowController : NSWindowController <NSLayoutManagerDelegate, NSTextViewDelegate>

@property ( assign ) TEScallingScrollView* _scrollView;
@property ( retain ) NSLayoutManager* _layoutMgr;

@property ( assign ) BOOL _hasMultiplePages;
@property ( assign ) BOOL _rulerIsBeingDisplayed;
@property ( assign ) BOOL _isSettingsSize;

// Convenience initializer. Loads the correct nib automatically.
- ( id ) init;

- ( NSUInteger ) numberOfPages;

- ( NSView* ) documentView;

- ( void ) breakUndoCoalescing;

#pragma mark -
#pragma mark Layout orientation sections
- ( NSArray* ) layoutOrientationSections;

- ( IBAction ) chooseAndAttachFiles: ( id )_Sender;

@end // TEDocumentWindowController

////////////////////////////////////////////////////////////////////////////

/****************************************************************************
 **                                                                        **
 **      _________                                      _______            **
 **     |___   ___|                                   / ______ \           **
 **         | |     _______   _______   _______      | /      |_|          **
 **         | |    ||     || ||     || ||     ||     | |    _ __           **
 **         | |    ||     || ||     || ||     ||     | |   |__  \          **
 **         | |    ||     || ||     || ||     ||     | \_ _ __| |  _       **
 **         |_|    ||_____|| ||     || ||_____||      \________/  |_|      **
 **                                           ||                           **
 **                                    ||_____||                           **
 **                                                                        **
 ***************************************************************************/
///:~