///:
/*****************************************************************************
 **                                                                         **
 **                               .======.                                  **
 **                               | INRI |                                  **
 **                               |      |                                  **
 **                               |      |                                  **
 **                      .========'      '========.                         **
 **                      |   _      xxxx      _   |                         **
 **                      |  /_;-.__ / _\  _.-;_\  |                         **
 **                      |     `-._`'`_/'`.-'     |                         **
 **                      '========.`\   /`========'                         **
 **                               | |  / |                                  **
 **                               |/-.(  |                                  **
 **                               |\_._\ |                                  **
 **                               | \ \`;|                                  **
 **                               |  > |/|                                  **
 **                               | / // |                                  **
 **                               | |//  |                                  **
 **                               | \(\  |                                  **
 **                               |  ``  |                                  **
 **                               |      |                                  **
 **                               |      |                                  **
 **                               |      |                                  **
 **                               |      |                                  **
 **                   \\    _  _\\| \//  |//_   _ \// _                     **
 **                  ^ `^`^ ^`` `^ ^` ``^^`  `^^` `^ `^                     **
 **                                                                         **
 **                       Copyright (c) 2014 Tong G.                        **
 **                          ALL RIGHTS RESERVED.                           **
 **                                                                         **
 ****************************************************************************/

#import "TECommon.h"
#import "TEDocument.h"
#import "TEDocumentWindowController.h"
#import "TETextEditDefaultsKeys.h"
#import "TETextEditError.h"

// TEDocumentWindowController + TEPrivate
@interface TEDocumentWindowController ( TEPrivate )

- ( void ) setDocument: ( TEDocument* )_Doc;    // Overridden with more specific type. Expects Document instance.

- ( void ) setupInitialTextViewSharedState;
- ( void ) setupTextViewForDocument;
- ( void ) setupWindowForDocument;
- ( void ) setupPagesViewForLayoutOrientation: ( NSTextLayoutOrientation )_Orientation;

- ( void ) updateForRichTextAndRulerState: ( BOOL )_Rich;
- ( void ) autosaveIfNeededThenToggleRich;
- ( void ) toggleRichWithNewFileType: ( NSString* )_FileType;

- ( void ) showRulerDelayed: ( BOOL )_Flag;

- ( void ) addPage;
- ( void ) removePage;

- ( NSTextView* ) firstTextView;

- ( void ) printInfoUpdated;

- ( void ) resizeWindowForViewSize: ( NSSize )_Size;
- ( void ) setHasMultiplePages: ( BOOL )_Pages force: ( BOOL )_Force;

@end // TEDocumentWindowController + TEPrivate

#pragma mark -
#pragma mark TEDocumentWindowController
@implementation TEDocumentWindowController

@synthesize _scrollView;
@synthesize _layoutMgr;

@synthesize _hasMultiplePages;
@synthesize _rulerIsBeingDisplayed;
@synthesize _isSettingsSize;

#pragma mark -
#pragma mark NSWindowController Overrides
- ( id ) init
    {
    if ( self = [ super initWithWindowNibName: @"TEDocumentWindow" ] )
        {
        self._layoutMgr = [ [ NSLayoutManager allocWithZone: [ self zone ] ] init ];

        [ self._layoutMgr setDelegate: self ];
        [ self._layoutMgr setAllowsNonContiguousLayout: YES ];
        }

    return self;
    }

- ( void ) dealloc
    {
    if ( [ self document ] )
        [ self setDocument: nil ];

    [ NOTIFICATION_CENTER removeObserver: self ];

    [ [ self firstTextView ] removeObserver: self forKeyPath: @"_backgroundColor" ];
    [ self._scrollView removeObserver: self forKeyPath: @"_scaleFactor" ];
    [ [ self._scrollView verticalScroller ] removeObserver: self forKeyPath: @"scrollerStyle" ];
    [ self._layoutMgr release ];

    [ self showRulerDelayed: NO ];

    [ super dealloc ];  // NSWindowController deallocates all the nib objects
    }

- ( void ) windowDidLoad
    {
    
    }

#pragma mark -
#pragma mark TEDocumentWindowController + TEPrivate

/* This method can be called in three defferent situations (number three is a special TextEdit case):
 *  1) When the window controller is created and set up with a new or opened document. ( !_OldDoc && _NewDoc )
 *  2) When the document is closed, and the controller is about to be destroyed ( _OldDoc && !_NewDoc )
 *  3) When the window controller is assigned to another document (a document has been opened and it takes the place of an automatically-created window).
 *     In that case this method is called twice. First as #2, second as #1
 */
- ( void ) setDocument: ( TEDocument* )_NewDoc
    {
    TEDocument* oldDoc = [ [ self document ] retain ];

    if ( oldDoc )
        {
        [ self._layoutMgr unbind: @"_hyphenationFactor" ];
        [ [ self firstTextView ] unbind: @"editable" ];
        }

    [ super setDocument: _NewDoc ];

    if ( _NewDoc )
        {
        [ self._layoutMgr bind: @"_hyphenationFactor"
                      toObject: self
                   withKeyPath: @"self.document._hyphenationFactor"
                       options: nil ];

        [ [ self firstTextView ] bind: @"editable"
                             toObject: self
                          withKeyPath: @"self.document._isReadOnly"
                              options: @{ NSValueTransformerNameBindingOption : NSNegateBooleanTransformerName } ];
        }

    if ( oldDoc != _NewDoc )
        {
        if ( oldDoc )
            {
            /* Remove layout manager from the old TADocument's text storage.
             * No need to retain as we already own the object.
             */
            [ [ oldDoc _textStorage ] removeLayoutManager: self._layoutMgr ];

            [ oldDoc removeObserver: self forKeyPath: @"_printInfo" ];
            [ oldDoc removeObserver: self forKeyPath: @"_isRichText" ];
            [ oldDoc removeObserver: self forKeyPath: @"_viewSize" ];
            [ oldDoc removeObserver: self forKeyPath: @"_hasMultiplePages" ];
            }

        if ( _NewDoc )
            {
            [ [ _NewDoc _textStorage ] addLayoutManager: self._layoutMgr ];

            if ( [ self isWindowLoaded ] )
                {
                [ self setHasMultiplePages: [ _NewDoc _hasMultiplePages ] force: NO ];
                [ self setupInitialTextViewSharedState ];
                [ self setupWindowForDocument ];

                if ( [ _NewDoc _hasMultiplePages ] )
                    [ self._scrollView setScaleFactor: [ [ self document ] scaleFactor ] adjustPopup: YES ];

                [ [ _NewDoc undoManager ] removeAllActions ];
                }

            [ _NewDoc addObserver: self forKeyPath: @"_printInfo" options: 0 context: nil ];
            [ _NewDoc addObserver: self forKeyPath: @"_isRichText" options: 0 context: nil ];
            [ _NewDoc addObserver: self forKeyPath: @"_viewSize" options: 0 context: nil ];
            [ _NewDoc addObserver: self forKeyPath: @"_hasMultiplePages" options: 0 context: nil ];
            }
        }

    [ oldDoc release ];
    }

- ( void ) setupInitialTextViewSharedState
    {

    }

- ( void ) setupTextViewForDocument
    {

    }

- ( void ) setupWindowForDocument
    {

    }

- ( void ) setupPagesViewForLayoutOrientation: ( NSTextLayoutOrientation )_Orientation
    {

    }

- ( void ) updateForRichTextAndRulerState: ( BOOL )_Rich
    {

    }

- ( void ) autosaveIfNeededThenToggleRich
    {

    }

- ( void ) toggleRichWithNewFileType: ( NSString* )_FileType
    {

    }

- ( void ) showRulerDelayed: ( BOOL )_Flag
    {

    }

- ( void ) addPage
    {

    }

- ( void ) removePage
    {

    }

- ( NSTextView* ) firstTextView
    {
    return [ [ self _layoutMgr ] firstTextView ];
    }

- ( void ) printInfoUpdated
    {

    }

- ( void ) resizeWindowForViewSize: ( NSSize )_Size
    {

    }

- ( void ) setHasMultiplePages: ( BOOL )_Pages force: ( BOOL )_Force
    {

    }

@end // TEDocumentWindowController

////////////////////////////////////////////////////////////////////////////

/****************************************************************************
 **                                                                        **
 **      _________                                      _______            **
 **     |___   ___|                                   / ______ \           **
 **         | |     _______   _______   _______      | /      |_|          **
 **         | |    ||     || ||     || ||     ||     | |    _ __           **
 **         | |    ||     || ||     || ||     ||     | |   |__  \          **
 **         | |    ||     || ||     || ||     ||     | \_ _ __| |  _       **
 **         |_|    ||_____|| ||     || ||_____||      \________/  |_|      **
 **                                           ||                           **
 **                                    ||_____||                           **
 **                                                                        **
 ***************************************************************************/
///:~