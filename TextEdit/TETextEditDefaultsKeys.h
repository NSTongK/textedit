///:
/*****************************************************************************
 **                                                                         **
 **                               .======.                                  **
 **                               | INRI |                                  **
 **                               |      |                                  **
 **                               |      |                                  **
 **                      .========'      '========.                         **
 **                      |   _      xxxx      _   |                         **
 **                      |  /_;-.__ / _\  _.-;_\  |                         **
 **                      |     `-._`'`_/'`.-'     |                         **
 **                      '========.`\   /`========'                         **
 **                               | |  / |                                  **
 **                               |/-.(  |                                  **
 **                               |\_._\ |                                  **
 **                               | \ \`;|                                  **
 **                               |  > |/|                                  **
 **                               | / // |                                  **
 **                               | |//  |                                  **
 **                               | \(\  |                                  **
 **                               |  ``  |                                  **
 **                               |      |                                  **
 **                               |      |                                  **
 **                               |      |                                  **
 **                               |      |                                  **
 **                   \\    _  _\\| \//  |//_   _ \// _                     **
 **                  ^ `^`^ ^`` `^ ^` ``^^`  `^^` `^ `^                     **
 **                                                                         **
 **                       Copyright (c) 2014 Tong G.                        **
 **                          ALL RIGHTS RESERVED.                           **
 **                                                                         **
 ****************************************************************************/

#import <Foundation/Foundation.h>

// Note that not all keys in this file are exposed in the UI.
// Some of them are not used at all, but are kept for reference.
NSString extern* const TETextEditRichText;
NSString extern* const TETextEditShowPageBreaks;
NSString extern* const TETextEditAddExtensionToNewPlainTextFiles;
NSString extern* const TETextEditWindowWidth;
NSString extern* const TETextEditWindowHeight;
NSString extern* const TETextEditPlainTextEncodingForRead;
NSString extern* const TETextEditPlainTextEncodingForWrite;
NSString extern* const TETextEditIgnoreRichText;
NSString extern* const TETextEditIgnoreHTML;
NSString extern* const TETextEditTabWidth;
NSString extern* const TETextEditForegroundLayoutToIndex;
NSString extern* const TETextEditOpenPanelFollowsMainWindow;
NSString extern* const TETextEditCheckSpellingAsYouType;
NSString extern* const TETextEditCheckGrammarWithSpelling;
NSString extern* const TETextEditCorrectSpellingAutomatically;
NSString extern* const TETextEditShowRuler;
NSString extern* const TETextEditSmartCopyPaste;
NSString extern* const TETextEditSmartQuotes;
NSString extern* const TETextEditSmartDashes;
NSString extern* const TETextEditSmartLinks;
NSString extern* const TETextEditDataDetectors;
NSString extern* const TETextEditTextReplacement;
NSString extern* const TETextEditSubstitutionsEnabledInRichTextOnly;
NSString extern* const TETextEditUseXHTMLDocType;
NSString extern* const TETextEditUseTransitionalDocType;
NSString extern* const TETextEditEmbeddedCSS;
NSString extern* const TETextEditUseInlineCSS;
NSString extern* const TETextEditHTMLEncoding;
NSString extern* const TETextEditPreserveWhitespace;
NSString extern* const TETextEditAutosavingDelay;
NSString extern* const TETextEditNumberPagesWhenPrinting;
NSString extern* const TETextEditWrapToFitWhenPrinting;
NSString extern* const TETextEditUseScreenFonts;

// Use different convention for the key values here, to be consistent with the keys in Document
NSString extern* const TETextEditAuthorProperty;
NSString extern* const TETextEditCompanyProperty;
NSString extern* const TETextEditCopyrightProperty;

NSString extern* const TETextEidtAppleMeasurementUnits;

////////////////////////////////////////////////////////////////////////////

/****************************************************************************
 **                                                                        **
 **      _________                                      _______            **
 **     |___   ___|                                   / ______ \           **
 **         | |     _______   _______   _______      | /      |_|          **
 **         | |    ||     || ||     || ||     ||     | |    _ __           **
 **         | |    ||     || ||     || ||     ||     | |   |__  \          **
 **         | |    ||     || ||     || ||     ||     | \_ _ __| |  _       **
 **         |_|    ||_____|| ||     || ||_____||      \________/  |_|      **
 **                                           ||                           **
 **                                    ||_____||                           **
 **                                                                        **
 ***************************************************************************/
///:~