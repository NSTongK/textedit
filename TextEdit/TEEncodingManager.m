///:
/*****************************************************************************
 **                                                                         **
 **                               .======.                                  **
 **                               | INRI |                                  **
 **                               |      |                                  **
 **                               |      |                                  **
 **                      .========'      '========.                         **
 **                      |   _      xxxx      _   |                         **
 **                      |  /_;-.__ / _\  _.-;_\  |                         **
 **                      |     `-._`'`_/'`.-'     |                         **
 **                      '========.`\   /`========'                         **
 **                               | |  / |                                  **
 **                               |/-.(  |                                  **
 **                               |\_._\ |                                  **
 **                               | \ \`;|                                  **
 **                               |  > |/|                                  **
 **                               | / // |                                  **
 **                               | |//  |                                  **
 **                               | \(\  |                                  **
 **                               |  ``  |                                  **
 **                               |      |                                  **
 **                               |      |                                  **
 **                               |      |                                  **
 **                               |      |                                  **
 **                   \\    _  _\\| \//  |//_   _ \// _                     **
 **                  ^ `^`^ ^`` `^ ^` ``^^`  `^^` `^ `^                     **
 **                                                                         **
 **                       Copyright (c) 2014 Tong G.                        **
 **                          ALL RIGHTS RESERVED.                           **
 **                                                                         **
 ****************************************************************************/

#import "TEEncodingManager.h"
#import "TECommon.h"

#pragma mark -
#pragma mark Notification Names
NSString* const TEEncodingManagerEncodingsListChangedNotification = @"EncodingsListChanged";

#pragma mark -
#pragma mark TEEncodingPopUpButtonCell
/* EncodingPopUpButtonCell is a subclass of NSPopUpButtonCell which provides 
 * the ability to automatically recompute its contents on changes to the encodings 
 * list. This allows sprinkling these around the app any have them automatically update 
 * themselves. Because we really only want to know when the cell's selectedItem is changed, 
 * we want to prevent the last item ("Customize...") from being selected.
 * In a nib file, to indicate that a default entry is wanted, the first menu item 
 * is given a tag of -1.
 */
#define SHARED_ENCODING_MANAGER [ TEEncodingManager sharedEncodingManager ]
@implementation TEEncodingPopUpButtonCell

- ( id ) initTextCell: ( NSString* ) _StringValue
            pullsDown: ( BOOL ) _PullDown
    {
    if ( self = [ super initTextCell:_StringValue pullsDown:_PullDown ] )
        {
        [ [ NSNotificationCenter defaultCenter ] addObserver: self
                                                    selector: @selector( encodingListChanged: )
                                                        name: TEEncodingManagerEncodingsListChangedNotification
                                                      object: nil ];
        [ SHARED_ENCODING_MANAGER setupPopUpCell: self
                                selectedEncoding: TEEncodingManagerNoStringEncoding
                                withDefaultEntry: NO ];
        }

    return self;
    }

- ( id ) initWithCoder: ( NSCoder* ) _Coder
    {
    if ( self = [ super initWithCoder: _Coder ] )
        {
        [ NOTIFICATION_CENTER addObserver: self
                                 selector: @selector( encodingListChanged: )
                                     name: TEEncodingManagerEncodingsListChangedNotification
                                   object: nil ];

        [ SHARED_ENCODING_MANAGER setupPopUpCell: self
                                selectedEncoding: TEEncodingManagerNoStringEncoding
                                withDefaultEntry: ( [self numberOfItems] > 0 && [[self itemAtIndex: 0] tag] == TEEncodingManagerWantsAutomaticTag ) ];
        }

    return self;
    }

- ( void ) dealloc
    {
    [ NOTIFICATION_CENTER removeObserver: self ];
    [ super dealloc ];
    }

/* Don't allow selecting the "Customize" item and the separator before it.
 * (Note that the customize item can be chosen and an action will be sent,
 * but the selection doesn't change to it.)
 */
- ( void ) selectItemAtIndex: ( NSInteger ) _Index
    {
    if ( _Index + 2 <= [self numberOfItems] )
        [ super selectItemAtIndex: _Index ];
    }

/* Update contents based on encodings list customization */
- ( void ) encodingsListChanged: ( NSNotification* ) _Notification
    {
    [ SHARED_ENCODING_MANAGER setupPopUpCell: self
                            selectedEncoding: [ [ [ self selectedItem ] representedObject ] unsignedIntegerValue ]

                            withDefaultEntry: ( [self numberOfItems ] > 0 && [[self itemAtIndex: 0] tag] == TEEncodingManagerWantsAutomaticTag ) ];
    }

@end // TEEncodingPopUpButtonCell

#pragma makr -
#pragma mark TEEncodingManager

@implementation TEEncodingManager

/* Manage single shared instance which both init and sharedEncodingManager methods return. */
static TEEncodingManager* kSharedEncodingManager = nil;

+ ( TEEncodingManager* ) sharedEncodingManager
    {
    return kSharedEncodingManager ? kSharedEncodingManager : [[TEEncodingManager alloc] init];
    }

- ( id ) init
    {
    // We just have one instance of the TEEncodingManager class, return that one instead
    if ( kSharedEncodingManager )
        [ self release ];
    else if ( self = [ super init ] )
        kSharedEncodingManager = self;

    return kSharedEncodingManager;
    }

- ( void ) dealloc
    {
    if ( self != kSharedEncodingManager )
        [ super dealloc ]; // Don't free the shared instance
    }

/* Sort using the equivalent Mac encoding as the major key.
 * Secondary key is the actual encoding value, which works well enough. 
 * We treat Unicode encodings as special case, putting them at top of the list.
 */
static int/* NSOrderedComparison */ encodingCompareHandler( void const* _FirstPtr, void const* _SecondPtr )
    {
    CFStringEncoding first = *( CFStringEncoding* )_FirstPtr;
    CFStringEncoding second = *( CFStringEncoding* )_SecondPtr;
    CFStringEncoding macEncodingForFirst = CFStringGetMostCompatibleMacStringEncoding( first );
    CFStringEncoding macEncodingForSecond = CFStringGetMostCompatibleMacStringEncoding( second );

    if ( first == second )
        return NSOrderedSame; // Should really never happen

    if ( macEncodingForFirst == kCFStringEncodingUnicode
            || macEncodingForSecond == kCFStringEncodingUnicode )
        {
        if ( macEncodingForSecond == macEncodingForFirst )
            return ( first > second ) ? NSOrderedDescending : NSOrderedAscending; // Both Unicode; compare second order

        return ( macEncodingForFirst == kCFStringEncodingUnicode ) ? NSOrderedAscending : NSOrderedDescending;  // First is Unicode
        }

    if ( (macEncodingForFirst > macEncodingForSecond)
            || ((macEncodingForFirst == macEncodingForSecond) && (first > second)) )
        return NSOrderedDescending;

    return NSOrderedAscending;
    }

/* Return a sorted list of all available string encodings. */
+ ( NSArray* ) allAvalilableStringEncodings
    {
    static NSMutableArray* allEncodings = nil;

    if ( !allEncodings ) // Built list of encodings, sorted, and including only those with human readable names
        {
        CFStringEncoding const* cfEncodings = CFStringGetListOfAvailableEncodings();
        CFStringEncoding* tmp = NULL;

        for ( int num = 0; cfEncodings[ num ] != kCFStringEncodingInvalidId; num++ )
            {
            tmp = malloc( sizeof(CFStringEncoding) * num );
            memcpy( tmp, cfEncodings, sizeof( CFStringEncoding ) * num );     // Copy the list
            qsort( tmp, num, sizeof( CFStringEncoding ), encodingCompareHandler );  // Sort it

            allEncodings = [ [ NSMutableArray alloc ] init ];
            for ( int cnt = 0; cnt < num; cnt++ )
                {
                NSStringEncoding nsEncoding = CFStringConvertEncodingToNSStringEncoding( tmp[ cnt ] );
                if ( nsEncoding && [ NSString localizedNameOfStringEncoding: nsEncoding ] )
                    [ allEncodings addObject: [ NSNumber numberWithUnsignedInteger: nsEncoding] ];
                }
            }

        free( tmp );
        }

    return allEncodings;
    }

/* Called once (when the UI is first brought up) to properly setup the encodings list in the "Customize Encodings List" panel. */
- ( void ) setupEncodingList
    {
    NSArray* allEncodings = [ [ self class ] allAvalilableStringEncodings ];
    NSInteger numEncodings = [ allEncodings count ];

    for ( NSInteger cnt = 0; cnt < numEncodings; cnt++ )
        {
        NSNumber* encodingNumber = allEncodings[ cnt ];
        NSStringEncoding encoding = (NSStringEncoding)[ encodingNumber unsignedIntegerValue ];
        NSString* encodingName = [ NSString localizedNameOfStringEncoding: encoding ];

        NSCell* cell = nil;
        if ( cnt >= [ self._encodingMatrix numberOfRows ] )
            [ self._encodingMatrix addRow ];

        cell = [ self._encodingMatrix cellAtRow:cnt column:0 ];
        [ cell setTitle: encodingName ];
        [ cell setRepresentedObject: encodingNumber ];
        }

    [ self._encodingMatrix sizeToCells ];
    [ self noteEncodingListChange:NO updateList:YES postNotification:NO ];
    }

/* This method initializeds the provided popup with list of encodings;
 * it also sets up the selected encoding as indicated and if includeDefaultItem is YES,
 * includes an initial item for selecting "Automatic" choice.
 * All encoding items have an NSNumber with the encoding (or TEEncodingManagerNoStringEncoding) as their representedObject
 */
- ( void ) setupPopUpCell: ( TEEncodingPopUpButtonCell* ) _Popup
         selectedEncoding: ( NSStringEncoding ) _SelectedEncoding
         withDefaultEntry: ( BOOL ) _IncludeDefaultItem
    {
    NSArray* encs = [ self enabledEncodings ];

    // Put the encodings in the popup
    [ _Popup removeAllItems ];

    // Put the initial "Automatic" item, if desired
    if ( _IncludeDefaultItem )
        {
        [ _Popup addItemWithTitle: NSLocalizedString( @"Automatic", @"Encoding popup entry indicating choice of encoding" ) ];
        [ [ _Popup itemAtIndex: 0 ] setRepresentedObject: [ NSNumber numberWithUnsignedInteger: TEEncodingManagerNoStringEncoding ] ];
        [ [ _Popup itemAtIndex: 0 ] setTag: TEEncodingManagerWantsAutomaticTag ]; // so that the default entry is inlcued again next time
        }

    // Make sure the initial selected encoding apears in the list
    if ( !_IncludeDefaultItem
            && ( _SelectedEncoding != TEEncodingManagerNoStringEncoding )
            && ![ encs containsObject: [NSNumber numberWithUnsignedInteger: _SelectedEncoding] ] )
        encs = [ encs arrayByAddingObject: [NSNumber numberWithUnsignedInteger: _SelectedEncoding] ];

    // Fill with encodings
    NSUInteger __block itemToSelect = 0;
    [ encs enumerateObjectsUsingBlock:
        ^( NSNumber* _Encoding, NSUInteger _Index, BOOL* _Stop )
            {
            NSStringEncoding encodingInCocoa = [ _Encoding unsignedIntegerValue ];

            [ _Popup addItemWithTitle: [NSString localizedNameOfStringEncoding: encodingInCocoa] ];
            [ [ _Popup lastItem ] setRepresentedObject: _Encoding ];
            [ [ _Popup lastItem ] setEnabled: YES ];

            if ( encodingInCocoa == _SelectedEncoding )
                itemToSelect = [ _Popup numberOfItems ] - 1;
            } ];

    // Add an optional separator and "Customize" item at end
    if ( [ _Popup numberOfItems ] > 0 )
        [ [_Popup menu] addItem: [NSMenuItem separatorItem] ];

    [ _Popup addItemWithTitle: NSLocalizedString( @"Customize Encoding List..."
                                                , @"Encoding popup entry for bringing up the Customize Encoding List panel "
                                                   "(this also occurs as the title of the panel itself, they should have the same localization)" ) ];
    [ [ _Popup lastItem ] setAction: @selector( showPanel: ) ];
    [ [ _Popup lastItem ] setTarget: self ];

    [ _Popup selectItemAtIndex: itemToSelect ];
    }

/* Returns the actual enabled list of encodings. */
- ( NSArray* ) enabledEncodings
    {
    NSInteger static const plainTextFileStringEncodingSupported[] =
        { kCFStringEncodingUnicode, kCFStringEncodingUTF8, kCFStringEncodingMacRoman
        , kCFStringEncodingWindowsLatin1, kCFStringEncodingMacJapanese, kCFStringEncodingShiftJIS
        , kCFStringEncodingMacChineseTrad, kCFStringEncodingMacKorean, kCFStringEncodingMacChineseSimp
        , kCFStringEncodingGB_18030_2000, -1
        };

    if ( self._encodings == nil )
        {
        NSMutableArray* encs = [ [ USER_DEFAULTS arrayForKey: TEEncodingManagerEncodingEncodings ] mutableCopy ];

        if ( !encs )    // If there is not any customized default by users
            {
            NSStringEncoding defaultEncoding = [ NSString defaultCStringEncoding ];
            NSStringEncoding encodingInCocoaString;
            BOOL hasDefault = NO;
            encs = [ [ NSMutableArray alloc ] init ];

            for ( NSInteger cnt = 0; plainTextFileStringEncodingSupported[ cnt ] != -1; cnt++ )
                {
                if ( (encodingInCocoaString = CFStringConvertEncodingToNSStringEncoding( (CFStringEncoding)plainTextFileStringEncodingSupported[ cnt ])) != kCFStringEncodingInvalidId )
                    {
                    [ encs addObject: [ NSNumber numberWithUnsignedInteger: encodingInCocoaString ] ];

                    if ( encodingInCocoaString == defaultEncoding )
                        hasDefault = YES;
                    }
                }

            if ( !hasDefault )
                [ encs addObject: [ NSNumber numberWithUnsignedInteger: defaultEncoding ] ];
            }

        self._encodings = encs;
        }

    return self._encodings;
    }

/* Should be called after any customization to the encoding list.
 * Writes the new list out to defaults; updates the UI;
 * also posts notification to get all encoding popups to update.
 */
- ( void ) noteEncodingListChange: ( BOOL ) _IsWriteDefault
                       updateList: ( BOOL ) _IsUpdateList
                 postNotification: ( BOOL ) _IsPost
    {
    if ( _IsWriteDefault )
        [ USER_DEFAULTS setObject:self._encodings forKey:TEEncodingManagerEncodingEncodings ];

    if ( _IsUpdateList )
        {
        NSInteger numEncodings = [ self._encodingMatrix numberOfRows ];

        for ( int cnt = 0; cnt < numEncodings; cnt++ )
            {
            NSCell* cell = [ self._encodingMatrix cellAtRow:cnt column:0 ];
            [ cell setState:[ self._encodings containsObject: [cell representedObject] ] ? NSOnState : NSOffState ];
            }
        }

    if ( _IsPost )
        [ NOTIFICATION_CENTER postNotificationName: TEEncodingManagerEncodingEncodingsListChangedNotification
                                            object: nil ];
    }

/* Because we want the encoding list to be modifiable even when a modal panel (such as the open panel) is up, 
 * we indicate that both the encodings list panel and the target work when modal. 
 * (See showPanel: below for the former...)
 */
- ( BOOL ) worksWhenModal
    {
    return YES;
    }

#pragma mark -
#pragma mark Action methods
- ( IBAction ) showPanel: ( id ) _Sender
    {
    if ( !self._encodingMatrix )
        {
        if ( ![ [ NSBundle mainBundle ] loadNibNamed: @"TESelectEncodingsPanel"
                                               owner: self
                                     topLevelObjects: nil ] )
            {
            NSLog( @"Failed to load TESelectEncodingsPanel.nib" );
            return;
            }

        [ [ self._encodingMatrix window ] retain ]; // loadNibNamed:owner:topLevelObjects: does not retain top level objects
        [ (NSPanel*)[ self._encodingMatrix window ] setWorksWhenModal: YES ];   // This should work when open panel is up
        [ [ self._encodingMatrix window ] setLevel: NSModalPanelWindowLevel ];  // Again, for the same reason
        [ self setupEncodingList ];     // Initialize the list (only need to do this once)
        }

    [ [ self._encodingMatrix window ] makeKeyAndOrderFront: nil ];
    }

- ( IBAction ) encodingListChanged: ( id ) _Sender
    {
    NSInteger numRows = [ self._encodingMatrix numberOfRows ];
    NSMutableArray* encs = [ [ NSMutableArray alloc ] init ];

    for ( NSInteger cnt = 0; cnt < numRows; cnt++ )
        {
        NSCell* cell = [ self._encodingMatrix cellAtRow:cnt column:0 ];
        NSNumber* encodingNumber = [ cell representedObject ];

        if ( ([encodingNumber unsignedIntegerValue] != TEEncodingManagerNoStringEncoding) && ([cell state] == NSOnState) )
            [ encs addObject: encodingNumber ];
        }

    [ self._encodings autorelease ];
    self._encodings = encs;

    [ self noteEncodingListChange:YES updateList:NO postNotification:YES ];
    }

- ( IBAction ) clearAll: ( id ) _Sender
    {
    [ self._encodings autorelease ];
    self._encodings = [ [ NSArray array ] retain ]; // Empty encoding list

    [ self noteEncodingListChange:YES updateList:YES postNotification:YES ];
    }

- ( IBAction ) selectAll: ( id ) _Sender
    {
    [ self._encodings autorelease ];
    self._encodings = [ [ [ self class ] allAvalilableStringEncodings ] retain ];

    [ self noteEncodingListChange:YES updateList:YES postNotification:YES ];
    }

- ( IBAction ) revertToDefault: ( id ) _Sender
    {
    [ self._encodings autorelease ];
    self._encodings = nil;

    [ USER_DEFAULTS removeObjectForKey: TEEncodingManagerEncodingEncodings ];

    (void)[ self enabledEncodings ];    // Regenerate default list

    [ self noteEncodingListChange:NO updateList:YES postNotification:YES ];
    }

@end // TEEncodingManager

////////////////////////////////////////////////////////////////////////////

/****************************************************************************
 **                                                                        **
 **      _________                                      _______            **
 **     |___   ___|                                   / ______ \           **
 **         | |     _______   _______   _______      | /      |_|          **
 **         | |    ||     || ||     || ||     ||     | |    _ __           **
 **         | |    ||     || ||     || ||     ||     | |   |__  \          **
 **         | |    ||     || ||     || ||     ||     | \_ _ __| |  _       **
 **         |_|    ||_____|| ||     || ||_____||      \________/  |_|      **
 **                                           ||                           **
 **                                    ||_____||                           **
 **                                                                        **
 ***************************************************************************/
///:~