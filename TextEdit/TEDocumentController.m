///:
/*****************************************************************************
 **                                                                         **
 **                               .======.                                  **
 **                               | INRI |                                  **
 **                               |      |                                  **
 **                               |      |                                  **
 **                      .========'      '========.                         **
 **                      |   _      xxxx      _   |                         **
 **                      |  /_;-.__ / _\  _.-;_\  |                         **
 **                      |     `-._`'`_/'`.-'     |                         **
 **                      '========.`\   /`========'                         **
 **                               | |  / |                                  **
 **                               |/-.(  |                                  **
 **                               |\_._\ |                                  **
 **                               | \ \`;|                                  **
 **                               |  > |/|                                  **
 **                               | / // |                                  **
 **                               | |//  |                                  **
 **                               | \(\  |                                  **
 **                               |  ``  |                                  **
 **                               |      |                                  **
 **                               |      |                                  **
 **                               |      |                                  **
 **                               |      |                                  **
 **                   \\    _  _\\| \//  |//_   _ \// _                     **
 **                  ^ `^`^ ^`` `^ ^` ``^^`  `^^` `^ `^                     **
 **                                                                         **
 **                       Copyright (c) 2014 Tong G.                        **
 **                          ALL RIGHTS RESERVED.                           **
 **                                                                         **
 ****************************************************************************/

#import "TECommon.h"
#import "TEDocumentController.h"
#import "TETextEditDefaultsKeys.h"
#import "TETextEditError.h"
#import "TEEncodingManager.h"

#pragma mark -
#pragma mark TEOpenSaveAccessoryOwner
/* A very simple container class which is used to collect the outlets from loading the encoding accessory.  
 * No implementation provided, because all of the references are weak and don't need retain/release. 
 * Would be nice to be able to switch to a mutable dictionary here at some point. */
@interface TEOpenSaveAccessoryOwner: NSObject

@property ( assign ) IBOutlet NSView* _accessoryView;
@property ( assign ) IBOutlet NSPopUpButton* _encodingPopUp;
@property ( assign ) IBOutlet NSButton* _checkBox;

@end // TEOpenSaveAccessoryOwner

@implementation TEOpenSaveAccessoryOwner
@end // TEOpenSaveAccessoryOwner

#pragma mark -
#pragma mark TEDocumentController
// TEDocumentController
@implementation TEDocumentController

@synthesize _customOpenSettings;

@synthesize _deferredDocuments;
@synthesize _transientDocumentLock;
@synthesize _displayDocumentLock;

- ( void ) awakeFromNib
    {
    [ self bind: @"autosavingDelay"
       toObject: [ NSUserDefaultsController sharedUserDefaultsController ]
    withKeyPath: [ @"values." stringByAppendingString: TETextEditAutosavingDelay ]
        options: nil ];

    self._customOpenSettings = [ [ NSMutableDictionary alloc ] init ];
    self._transientDocumentLock = [ [ NSLock alloc ] init ];
    self._displayDocumentLock = [ [ NSLock alloc ] init ];
    }

- ( void ) dealloc
    {
    [ self unbind: @"autosavingDelay" ];
    [ self._customOpenSettings release ];
    [ self._transientDocumentLock release ];
    [ self._displayDocumentLock release ];
    }

/* Create a new document of the default type and initialize its contents from the pasteboard. */
- ( TEDocument* ) openDocumentWithContentsOfPasteboard: ( NSPasteboard* ) _Pasteboard
                                               display: ( BOOL ) _Display
                                                 error: ( NSError** ) _OutError
    {
    // Read type and attributed string.
    NSString* pasteboardType =
        [ _Pasteboard availableTypeFromArray: [ NSAttributedString readableTypesForPasteboard: _Pasteboard ] ];

    NSData* data = [ _Pasteboard dataForType: pasteboardType ];
    NSAttributedString* string = nil;
    NSString* type = nil;

    if ( !data )
        {
        NSDictionary* attributes = nil;
        string = [ [ [ NSAttributedString alloc ] initWithData: data
                                                       options: nil
                                            documentAttributes: &attributes
                                                         error: _OutError ] autorelease ];

        // We only expect to see plain-text, RTF, and RTFD at this point.
        NSString* docType = attributes[ NSDocumentTypeDocumentAttribute ];

        if ( [ docType isEqualToString: NSPlainTextDocumentType ] )     type = (NSString*)kUTTypeText;
        else if ( [ docType isEqualToString: NSRTFTextDocumentType ] )  type = (NSString*)kUTTypeRTF;
        else if ( [ docType isEqualToString: NSRTFDTextDocumentType ] ) type = (NSString*)kUTTypeRTFD;
        }

    if ( string && type )
        {
        Class docClass = [ self documentClassForType: type ];

        if ( docClass )
            {
            TEDocument* transientDoc = nil;

            [ self._transientDocumentLock lock ];
            transientDoc = [ self transientDocumentToReplace ];

            if ( transientDoc )
                // If this document has claimed the transient document,
                // cause -transientDocumentToReplace to return nil for all other documents.
                [ transientDoc set_isTransient: NO ];
            [ self._transientDocumentLock unlock ];

            id doc = [ [ [ docClass alloc ] initWithType: type error: _OutError ] autorelease ];
            if ( !doc ) return nil; // error has been set

            NSTextStorage* textStorage = [ doc _textStorage ];
            [ textStorage replaceCharactersInRange: NSMakeRange( 0, [ textStorage length ] )
                              withAttributedString: string ];

            if ( [ type isEqualToString: (NSString*)kUTTypeText ] )
                [ doc applyDefaultTextAttributes: NO ];

            [ self addDocument: doc ];
            [ doc updateChangeCount: NSChangeReadOtherContents ];

            if ( transientDoc )
                [ self replaceTransientDocument: @[ transientDoc, doc ] ];

            if ( _Display )
                [ self displayDocument: doc ];

            return doc;
            }
        }

    // Either we could not read data from pastboard,
    // or the data was interpreted with a type we don't understand.
    if ( (!data || (string && !type)) && _OutError )
        *_OutError = [ NSError errorWithDomain: TETextEditErrorDomain
                                          code: TETextEditOpenDocumentWithSelectionServiceFailed
                                      userInfo:
        @{ NSLocalizedDescriptionKey : NSLocalizedString( @"Service failed. Couldn't open the selection."
                                                        , @"Title of alert indicating error during 'New Window Containing Selection' service" )
         , NSLocalizedDescriptionKey : NSLocalizedString( @"There might be an internal error or a performance problem, or the source application may be providing text of invalid type in service request. Please try the operation a second time. If that doesn't work, copy/paste the selection into TextEdit."
                                                        , @"Recommendation when 'New Window Containing Selection' service fails" )
         } ];

    return nil;
    }

/* This method is overridden in order to support transient documents, 
 * i.e. the automatic closing of an automatically created untitled document, 
 * when a real document is opened. */
- ( id ) openUntitledDocumentAndDisplay: ( BOOL ) _DisplayDocument
                                  error: ( NSError** ) _OutError
    {
    TEDocument* doc = [ super openUntitledDocumentAndDisplay: _DisplayDocument
                                                       error: _OutError ];
    if ( !doc )
        return nil;

    if ( [ [ self documents ] count ] == 1 )
        {
        // Determine whether this document might be a transient one
        // Check if there is a current AppleEvent.
        // If there is, check whether it is an open or reopen event.
        // In that case, the document being created is transient.
        NSAppleEventDescriptor* eventDescriptor = [ [ NSAppleEventManager sharedAppleEventManager ] currentAppleEvent ];
        AEEventID eventID = [ eventDescriptor eventID ];

        if ( eventDescriptor && (eventID == kAEReopenApplication || eventID == kAEOpenApplication)
                             && [ eventDescriptor eventClass ] == kCoreEventClass )
            [ doc set_isTransient: YES ];
        }

    return doc;
    }

- ( TEDocument* ) transientDocumentToReplace
    {
    NSArray* documents = [ self documents ];
    TEDocument* transientDoc = nil;

    return ( [ documents count ] == 1
                && [ (transientDoc = documents.firstObject) isTransientAndCanBeReplaces ] ) ? transientDoc : nil;
    }

- ( void ) displayDocument: ( NSDocument* ) _Document
    {
    // Documents must be displayed on the main tread.
    if ( [ NSThread isMainThread ] )
        {
        [ _Document makeWindowControllers ];
        [ _Document showWindows ];
        }
    else
        [ self performSelectorOnMainThread: _cmd
                                withObject: _Document
                             waitUntilDone: YES ];
    }

- ( void ) replaceTransientDocument: ( NSArray* ) _Documents
    {
    // Transient document must be replaced on the main thread,
    // since it may undergo automatic display on the main thread.
    if ( [ NSThread isMainThread ] )
        {
        NSDocument* transientDoc = [ _Documents firstObject ];
        NSDocument* doc = [ _Documents objectAtIndex: 1 ];
        NSArray* controllersToTransfer = [ [ transientDoc windowControllers ] copy ];
        NSEnumerator* controllerEnum = [ controllersToTransfer objectEnumerator ];
        NSWindowController* controller;

        [ controllersToTransfer makeObjectsPerformSelector: @selector( retain ) ];

        while ( controller = [ controllerEnum nextObject ] )
            {
            [ doc addWindowController: controller ];
            [ transientDoc removeWindowController: controller ];
            }

        [ transientDoc close ];

        [ controllersToTransfer makeObjectsPerformSelector: @selector( release ) ];
        [ controllersToTransfer release ];

        for ( NSLayoutManager* layoutManager in [ [ (TEDocument*)doc _textStorage ] layoutManagers ] )
            {
            for ( NSTextContainer* textContainer in [ layoutManager textContainers ] )
                {
                NSTextView* textView = [ textContainer textView ];

                if ( textView )
                    NSAccessibilityPostNotification( textView, NSAccessibilityValueChangedNotification );
                }
            }
        }
    else
        [ self performSelectorOnMainThread: _cmd
                                withObject: _Documents
                             waitUntilDone: YES ];
    }

// When a document is opened, check to see whether there is a document that is already open, and whether it is transient.
// If so, transfer the document's window controllers and close the transient document.
// When +[Document canConcurrentlyReadDocumentsOfType:] return YES, this method may be invoked on multiple threads.
// Ensure that only one document replaces the transient document. 
// The transient document must be replaced before any other documents are displayed for window cascading to work correctly.
// To guarantee this, defer all display operations until the transient document has been replaced.
- ( id ) openDocumentWithContentsOfURL: ( NSURL* ) _AbsoluteURL
                               display: ( BOOL ) _IsDisplayDocument
                                 error: ( NSError** ) _OutError
    {
    TEDocument* transientDoc = nil;

    [ self._transientDocumentLock lock ];
    transientDoc = [ self transientDocumentToReplace ];

    if ( transientDoc )
        {
        // Once this document has clainmed the transient document,
        // cause -transientDocumentToReplace to return nil for all other documents.
        [ transientDoc set_isTransient: NO ];
        self._deferredDocuments = [ [ NSMutableArray alloc ] init ];
        }

    [ self._transientDocumentLock unlock ];

    // Don't make NSDocumentController display the NSDocument it creates.
    // Instead, do it later manually to ensure that the transient document has been replacesd first.
    TEDocument* doc = [ super openDocumentWithContentsOfURL: _AbsoluteURL
                                                    display: _IsDisplayDocument
                                                      error: _OutError ];

    [ self._customOpenSettings removeObjectForKey: _AbsoluteURL ];

    if ( transientDoc )
        {
        if ( doc )
            {
            [ self replaceTransientDocument: @[ transientDoc, doc ] ];

            if ( _IsDisplayDocument )
                [ self displayDocument: doc ];
            }

        // Now that the transient document has been replaced, display all deferred document.
        [ self._displayDocumentLock lock ];
        NSArray* documentsToDisplay = self._deferredDocuments;
        self._deferredDocuments = nil;
        [ self._displayDocumentLock unlock ];

        for ( NSDocument* document in documentsToDisplay )
            [ self displayDocument: document ];

        [ documentsToDisplay release ];
        }
    else if ( doc && _IsDisplayDocument )
        {
        [ self._displayDocumentLock lock ];
        if ( self._deferredDocuments )
            {
            // Defer displaying this document, because the transient document has not yet replaced.
            [ self._deferredDocuments addObject: doc ];
            [ self._displayDocumentLock lock ];
            }
        else
            {
            // The transient document has been replaced, so display the document immediately.
            [ self._displayDocumentLock unlock ];
            [ self displayDocument: doc ];
            }
        }

    return doc;
    }

/* When a second document is added, the first document's transient is cleared.
 * This happens when the user selects "New" when a transient document already exists.
 */
- ( void ) addDocument: ( NSDocument* ) _NewDoc
    {
    TEDocument* firstDoc = nil;
    NSArray* documents = [ self documents ];

    if ( [ documents count ] == 1 && (firstDoc = [ documents firstObject ])
                                  && [ firstDoc _isTransient ] )
        [ firstDoc set_isTransient: NO ];

    [ super addDocument: _NewDoc ];
    }

/* Loads the "encoding" accessory view used in save plain and open panels.
 * There is a checkbox in the accessory which has difference purposes in each case;
 * so we let caller set the title and other info for that checkbox. */
+ ( NSView* ) encodingAccessory: ( NSStringEncoding ) _Encoding
            includeDefaultEntry: ( BOOL ) _IncludeDefaultItem
                  encodingPopup: ( NSPopUpButton** ) _Popup
                       checkBox: ( NSButton** ) _Button
    {
    TEOpenSaveAccessoryOwner* owner = [ [ [ TEOpenSaveAccessoryOwner alloc ] init ] autorelease ];

    // Rather than caching, load the accessory view everytime, as it might apear in multiple panels.
    if ( ![ [ NSBundle mainBundle ] loadNibNamed: @"TEEncodingAccessory"
                                           owner: owner
                                 topLevelObjects: nil ] )
        {
        NSLog( @"Failed to load TEEncodingAccessory.nib" );
        return nil;
        }

    if ( _Popup )  *_Popup = owner._encodingPopUp;
    if ( _Button ) *_Button = owner._checkBox;

    [ [ TEEncodingManager sharedEncodingManager ] setupPopUpCell: [ owner._encodingPopUp cell ]
                                                selectedEncoding: _Encoding
                                                withDefaultEntry: _IncludeDefaultItem ];
    return owner._accessoryView;
    }

/* Overriden to add an accessory view to the open panel.
 * This method is called for both modal and non-modal invocations.
 */
- ( void ) beginOpenPanel: ( NSOpenPanel* ) _OpenPanel
                 forTypes: ( NSArray* ) _InTypes
        completionHandler: ( void (^)(NSInteger reslt) ) _CompletionHandler
    {
    NSPopUpButton* encodingPopUp = nil;
    NSButton* ignoreRichTextButton = nil;

    BOOL ignoreHTMLOrig = [ USER_DEFAULTS boolForKey: TETextEditIgnoreHTML ];
    BOOL ignoreRichOrig = [ USER_DEFAULTS boolForKey: TETextEditIgnoreRichText ];

    NSView* accessoryView = [ [ self class ] encodingAccessory: [ [ USER_DEFAULTS objectForKey: TETextEditPlainTextEncodingForRead ] unsignedIntegerValue ]
                                           includeDefaultEntry: YES
                                                 encodingPopup: &encodingPopUp
                                                      checkBox: &ignoreRichTextButton ];

    accessoryView.translatesAutoresizingMaskIntoConstraints = NO;

    [ _OpenPanel setAccessoryView: accessoryView ];
    [ ignoreRichTextButton setTitle: NSLocalizedString( @"Ignore rich text commands"
                                                      , @"Checkbox inidicated that when opeing a rich text file, the rich text should be ignored (causing the file to be loaded as plain text)" ) ];
    [ ignoreRichTextButton setToolTip: NSLocalizedString( @"If selected, HTML and RTF files will be loaded as plain text, allowing you to see and edit the HTML of RTF directives."
                                                        , @"Tooltip for checkbox indicating that when opening a rich text file, the rich text should be ignored (causing the file to be loaded as plain text)" ) ];
    if ( ignoreRichOrig != ignoreHTMLOrig )
        {
        [ ignoreRichTextButton setAllowsMixedState: YES ];
        [ ignoreRichTextButton setState: NSMixedState ];
        }
    else
        {
        if ( [ignoreRichTextButton allowsMixedState] )
            [ ignoreRichTextButton setAllowsMixedState: NO ];

        [ ignoreRichTextButton setState: (ignoreRichOrig ? NSOnState : NSOffState) ];
        }

    [ super beginOpenPanel:_OpenPanel forTypes:_InTypes completionHandler:
        ^( NSInteger _Result )
            {
            if ( _Result == NSOKButton )
                {
                BOOL ignoreHTML = ignoreHTMLOrig;
                BOOL ignoreRich = ignoreRichOrig;

                NSStringEncoding encoding = (NSStringEncoding)[[[encodingPopUp selectedItem] representedObject] unsignedIntegerValue];
                NSCellStateValue ignoreState = [ ignoreRichTextButton state ];

                if ( ignoreState != NSMixedState )  // Mixed state indicates they were different, and to leave them alone
                    ignoreHTML = ignoreRich = (ignoreState == NSOnState);

                NSDictionary* optionsDict = @{ TETextEditPlainTextEncodingForRead : [NSNumber numberWithUnsignedInteger:encoding]
                                             , TETextEditIgnoreRichText : [NSNumber numberWithBool:ignoreHTML]
                                             , TETextEditRichText : [NSNumber numberWithBool:ignoreRich]
                                             };

                for ( NSURL* url in [_OpenPanel URLs] )
                    self._customOpenSettings[ url ] = optionsDict;
                }

            _CompletionHandler( _Result );
            } ];
    }

- ( NSStringEncoding ) lastSelectedEncodingForURL: ( NSURL* ) _URL
    {
    NSDictionary* optionsDict = self._customOpenSettings[ _URL ];

    return optionsDict ? [ optionsDict[ TETextEditPlainTextEncodingForRead ] unsignedIntegerValue ]
                       : [ [USER_DEFAULTS objectForKey:TETextEditPlainTextEncodingForRead] unsignedIntegerValue ];
    }

- ( BOOL ) lastSelectedIgnoreHTMLForURL: ( NSURL* ) _URL
    {
    NSDictionary* optionsDict = self._customOpenSettings[ _URL ];

    return optionsDict ? [ optionsDict[ TETextEditIgnoreHTML ] unsignedIntegerValue ]
                       : [ [USER_DEFAULTS objectForKey:TETextEditIgnoreHTML] unsignedIntegerValue ];
    }

- ( BOOL ) lastSelectedIgnoreRichForURL: ( NSURL* ) _URL
    {
    NSDictionary* optionsDict = self._customOpenSettings[ _URL ];

    return optionsDict ? [ optionsDict[ TETextEditIgnoreRichText ] unsignedIntegerValue ]
                       : [ [USER_DEFAULTS objectForKey:TETextEditIgnoreRichText] unsignedIntegerValue ];
    }

/* The user can change the default document type between Rich and Plain in Preference.
 * We override -defaultType to return the appropriate type string.
 */
- ( NSString* ) defaultType
    {
    return (NSString*)( [USER_DEFAULTS boolForKey:TETextEditRichText] ? kUTTypeRTF : kUTTypeText );
    }

@end // TEDocumentController

////////////////////////////////////////////////////////////////////////////

/****************************************************************************
 **                                                                        **
 **      _________                                      _______            **
 **     |___   ___|                                   / ______ \           **
 **         | |     _______   _______   _______      | /      |_|          **
 **         | |    ||     || ||     || ||     ||     | |    _ __           **
 **         | |    ||     || ||     || ||     ||     | |   |__  \          **
 **         | |    ||     || ||     || ||     ||     | \_ _ __| |  _       **
 **         |_|    ||_____|| ||     || ||_____||      \________/  |_|      **
 **                                           ||                           **
 **                                    ||_____||                           **
 **                                                                        **
 ***************************************************************************/
///:~